export interface Socket {
    onmessage: ((msg: string) => any) | null;
    send: (data: string) => void;
}
/**
 * Executes the given query.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the variables to be substituted
 * @returns a promise resolving to the result of the query
 */
export declare function query(queryStr: string, variables: object): Promise<any>;
/**
 * Executes the given query.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the variables to be substituted
 * @returns the async iterable resulting from the subscription
 */
export declare function subscription(queryStr: string, variables: object): AsyncIterableIterator<any>;
/**
 * Executes a query over HTTP.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the query variables
 * @param signature - the hmac of the query string
 * @returns a promise resolving to the result of the query
 */
export declare function http(queryStr: string, variables: object): Promise<any>;
/**
 * Executes a query over a websocket.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the query variables
 * @param signature - the hmac of the query string
 * @returns an async iterable that yields all received messages (so that
 *     we may continuously receive messages from a subscription)
 */
export declare function ws<T, V>(queryStr: string, variables: V): AsyncIterableIterator<T>;
export declare enum SensorType {
    YES_NO = "YES_NO",
    COUNT = "COUNT"
}
export declare enum UserType {
    END_USER = "END_USER",
    WORKER = "WORKER",
    WORKER_MANAGER = "WORKER_MANAGER"
}
export declare enum CollectionProtocol {
    FTP = "FTP",
    HTTP_UPLOAD = "HTTP_UPLOAD",
    HTTP_POLLING = "HTTP_POLLING"
}

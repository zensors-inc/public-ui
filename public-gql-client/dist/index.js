"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var nconf_1 = require("nconf");
var nconf = new nconf_1.Provider();
nconf
    .argv()
    .env()
    .defaults({
    GQL_HOSTNAME: "localhost",
    GQL_PORT: 3000,
    GQL_HTTP_PROTOCOL: "http",
    GQL_WS_PROTOCOL: "ws",
});
/**
 * Returns the HTTP URL of the GQL server.
 *
 * @returns the HTTP URL of the GQL server
 */
function getHttpUrl() {
    return nconf.get("GQL_HTTP_PROTOCOL") + "://" + nconf.get("GQL_HOSTNAME") + ":" + nconf.get("GQL_PORT") + "/public-api/graphql";
}
/**
 * Returns the WS URL of the GQL server.
 *
 * @returns the WS URL of the GQL server
 */
function getWsUrl() {
    return nconf.get("GQL_WS_PROTOCOL") + "://" + nconf.get("GQL_HOSTNAME") + ":" + nconf.get("GQL_PORT")
        + "/public-api/graphql-subscription";
}
/**
 * Executes the given query.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the variables to be substituted
 * @returns a promise resolving to the result of the query
 */
function query(queryStr, variables) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, http(queryStr, variables)];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.query = query;
/**
 * Executes the given query.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the variables to be substituted
 * @returns the async iterable resulting from the subscription
 */
function subscription(queryStr, variables) {
    return ws(queryStr, variables);
}
exports.subscription = subscription;
/**
 * Executes a query over HTTP.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the query variables
 * @param signature - the hmac of the query string
 * @returns a promise resolving to the result of the query
 */
function http(queryStr, variables) {
    return __awaiter(this, void 0, void 0, function () {
        var payload, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    payload = {
                        query: queryStr,
                        variables: variables
                    };
                    return [4 /*yield*/, fetch(getHttpUrl(), {
                            body: JSON.stringify(payload),
                            credentials: "include",
                            headers: {
                                "Content-Type": "application/json"
                            },
                            method: "POST"
                        })];
                case 1:
                    response = _a.sent();
                    if (!response.ok) return [3 /*break*/, 3];
                    return [4 /*yield*/, response.json()];
                case 2: return [2 /*return*/, (_a.sent()).data];
                case 3: throw new Error(response.statusText);
            }
        });
    });
}
exports.http = http;
/**
 * Obtains a websocket connected to the given URL.
 *
 * @param url - the URL to which to connect
 * @returns a promise resolving in the websocket connection or rejecting with
 *     the encountered error
 */
function getSocket(url) {
    return new Promise(function (resolve, reject) {
        var websocket = new WebSocket(url);
        var impl = {
            onmessage: null,
            send: websocket.send
        };
        websocket.onopen = function () {
            resolve();
        };
        websocket.onerror = function (err) {
            reject(err);
        };
        websocket.onmessage = function (msg) {
            if (impl.onmessage) {
                impl.onmessage(msg.data);
            }
        };
    });
}
/**
 * Executes a query over a websocket.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the query variables
 * @param signature - the hmac of the query string
 * @returns an async iterable that yields all received messages (so that
 *     we may continuously receive messages from a subscription)
 */
function ws(queryStr, variables) {
    return __asyncGenerator(this, arguments, function ws_1() {
        function promiseOnce() {
            return new Promise(function (resolve) {
                socket.onmessage = function (msg) {
                    resolve(JSON.parse(msg));
                };
            });
        }
        var payload, socket;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    payload = {
                        query: queryStr,
                        variables: variables
                    };
                    return [4 /*yield*/, __await(getSocket(getWsUrl()))];
                case 1:
                    socket = _a.sent();
                    socket.send(JSON.stringify(payload));
                    _a.label = 2;
                case 2:
                    if (!true) return [3 /*break*/, 6];
                    return [4 /*yield*/, __await(promiseOnce())];
                case 3: return [4 /*yield*/, __await.apply(void 0, [(_a.sent())])];
                case 4: return [4 /*yield*/, _a.sent()];
                case 5:
                    _a.sent();
                    return [3 /*break*/, 2];
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.ws = ws;
var SensorType;
(function (SensorType) {
    SensorType["YES_NO"] = "YES_NO";
    SensorType["COUNT"] = "COUNT";
})(SensorType = exports.SensorType || (exports.SensorType = {}));
var UserType;
(function (UserType) {
    UserType["END_USER"] = "END_USER";
    UserType["WORKER"] = "WORKER";
    UserType["WORKER_MANAGER"] = "WORKER_MANAGER";
})(UserType = exports.UserType || (exports.UserType = {}));
var CollectionProtocol;
(function (CollectionProtocol) {
    CollectionProtocol["FTP"] = "FTP";
    CollectionProtocol["HTTP_UPLOAD"] = "HTTP_UPLOAD";
    CollectionProtocol["HTTP_POLLING"] = "HTTP_POLLING";
})(CollectionProtocol = exports.CollectionProtocol || (exports.CollectionProtocol = {}));

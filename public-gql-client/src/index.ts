"use strict";

import { Provider } from "nconf";

const nconf = new Provider();

nconf
	.argv()
	.env()
	.defaults({
		GQL_HOSTNAME: "localhost",
		GQL_PORT: 3000,
		GQL_HTTP_PROTOCOL: "http",
		GQL_WS_PROTOCOL: "ws",
	});

export interface Socket {
	onmessage: ((msg: string) => any) | null;
	send: (data: string) => void;
}

/**
 * Returns the HTTP URL of the GQL server.
 *
 * @returns the HTTP URL of the GQL server
 */
function getHttpUrl(): string {
	return `${nconf.get("GQL_HTTP_PROTOCOL")}://${nconf.get("GQL_HOSTNAME")}:${nconf.get("GQL_PORT")}/public-api/graphql`;
}

/**
 * Returns the WS URL of the GQL server.
 *
 * @returns the WS URL of the GQL server
 */
function getWsUrl(): string {
	return `${nconf.get("GQL_WS_PROTOCOL")}://${nconf.get("GQL_HOSTNAME")}:${nconf.get("GQL_PORT")}`
		+ `/public-api/graphql-subscription`;
}

/**
 * Executes the given query.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the variables to be substituted
 * @returns a promise resolving to the result of the query
 */
export async function query(queryStr: string, variables: object): Promise<any> {
	return await http(queryStr, variables);
}

/**
 * Executes the given query.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the variables to be substituted
 * @returns the async iterable resulting from the subscription
 */
export function subscription(queryStr: string, variables: object): AsyncIterableIterator<any> {
	return ws(queryStr, variables);
}

/**
 * Executes a query over HTTP.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the query variables
 * @param signature - the hmac of the query string
 * @returns a promise resolving to the result of the query
 */
export async function http(queryStr: string, variables: object): Promise<any> {
	let payload = {
		query: queryStr,
		variables
	};

	let response = await fetch(getHttpUrl(), {
		body: JSON.stringify(payload),
		credentials: "include",
		headers: {
			"Content-Type": "application/json"
		},
		method: "POST"
	});

	if (response.ok) {
		return (await response.json()).data;
	} else {
		throw new Error(response.statusText);
	}
}

/**
 * Obtains a websocket connected to the given URL.
 *
 * @param url - the URL to which to connect
 * @returns a promise resolving in the websocket connection or rejecting with
 *     the encountered error
 */
function getSocket(url: string): Promise<Socket> {
	return new Promise((resolve, reject) => {
		let websocket = new WebSocket(url);
		let impl: Socket = {
			onmessage: null,
			send: websocket.send
		};

		websocket.onopen = () => {
			resolve();
		};

		websocket.onerror = (err) => {
			reject(err);
		};

		websocket.onmessage = (msg) => {
			if (impl.onmessage) {
				impl.onmessage(msg.data as string);
			}
		};
	});
}

/**
 * Executes a query over a websocket.
 *
 * @param queryStr - the stringified query to execute
 * @param variables - the query variables
 * @param signature - the hmac of the query string
 * @returns an async iterable that yields all received messages (so that
 *     we may continuously receive messages from a subscription)
 */
export async function* ws<T, V>(queryStr: string, variables: V): AsyncIterableIterator<T> {
	let payload = {
		query: queryStr,
		variables
	};

	let socket = await getSocket(getWsUrl());

	function promiseOnce(): Promise<T> {
		return new Promise((resolve) => {
			socket.onmessage = (msg) => {
				resolve(JSON.parse(msg));
			};
		});
	}

	socket.send(JSON.stringify(payload));

	while (true) {
		yield (await promiseOnce());
	}
}

export enum SensorType {
	YES_NO = "YES_NO",
	COUNT = "COUNT"
}

export enum UserType {
	END_USER = "END_USER",
	WORKER = "WORKER",
	WORKER_MANAGER = "WORKER_MANAGER"
}

export enum CollectionProtocol {
	FTP = "FTP",
	HTTP_UPLOAD = "HTTP_UPLOAD",
	HTTP_POLLING = "HTTP_POLLING"
}


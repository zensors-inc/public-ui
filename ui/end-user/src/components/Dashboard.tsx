"use strict";

import "./Dashboard.scss";

import * as React from "react";

import { navigate } from "../actions/navigation-actions";

import { SensorType } from "@zensors/public-gql-client";

import { SensorDetails } from "../stores/SensorStore";

import ComparisonGrid from "./ComparisonGrid";

import DashboardStore from "../stores/DashboardStore";

import QuestionListTable from "./QuestionListTable";

import SearchFilter from "./SearchFilter";

import SensorListStore, { SensorListing } from "../stores/SensorListStore";

import ZensorGraph from "./ZensorGraph";


type Props = {};

type State = {
	// delsensors?: SensorListing[];
	sensors?: SensorDetails[];
	uids: string[];
	// filteredSensors?: SensorListing[];
	drawerVisible: boolean;
};

export default class Dashboard extends React.Component<{}, State> {
	public constructor(props: Props, context: any) {
		super(props, context);
		this.state = this.getState();
	}
	public componentDidMount() {
		DashboardStore.addSensorListUpdateListener(() => this.updateState());
	}

	public componentWillUnmount() {
		DashboardStore.removeSensorListUpdateListener(() => this.updateState());
	}

	public getState(): State {
		return {
			sensors: DashboardStore.visualizationSensors,
			uids: DashboardStore.uids,
			drawerVisible: false
		};
	}

	public updateState(): void {
		this.setState(this.getState());
	}

	public render() {
		let sensors;

		if (this.state.sensors && this.state.sensors.length > 0) {
			// Leaving this here, assuming we get the data to make the dashboard from the sensor
			sensors = this.state.sensors.map((sensor) =>
								<ZensorGraph
									chartName={sensor.name}
									// Timestamps should be normalized and parsed
									xSeries={[5, 2, 5, 2, 5, 2]}
									// Doesnt have any actual data yet, so just doing this with timestamps now
									ySeries={[0, 1, 2, 3, 4, 5]}
									graphType="line"
								/>
		);
		} else {
			sensors = (<li> No sensors </li>);
		}
		return (
			<div>
				<div className="drawer-overlay">
					<div className={(this.state.drawerVisible) ? "zs-dash-drawer" : "zs-dash-drawer-hidden"}>
						<h1> Add a new widget </h1>
						<QuestionListTable/>
					</div>
				</div>
				<div className="container">
					<div className="LocationDash">
					<h1>Beautyshoppe lawrenceville </h1>
					<a onClick={() => this.setState({drawerVisible: !this.state.drawerVisible})}>
						Add a new visualization
					</a>
					<div className="location-data-grid">
						{sensors}
						<ComparisonGrid
							comparisons={[
								{datapoint: "7.5%", underline: "Year over year"},
								{datapoint: "7.5%", underline: "Year over year"},
								{datapoint: "7.5%", underline: "Year over year"},
								{datapoint: "7.5%", underline: "Year over year"}
							]}
						/>
						<ZensorGraph
							chartName="Historical growth over 4 months"
							xSeries={[1, 2, 3, 4, 5, 6]}
							ySeries={[1, 7, 8, 0, 4, 2]}
							comparisons={[
							{underline: "Growth over year", datapoint: "+7.5%"},
							{underline: "Growth over year", datapoint: "+7.5%"}]}
							graphType="line"
						/>

						<ZensorGraph
							chartName="Snapshot taken today"
							xSeries={[1, 2, 3, 4, 5, 6]}
							ySeries={[1, 7, 8, 0, 4, 2]}
							comparisons={[
								{underline: "Growth over year", datapoint: "+7.5%"},
								{underline: "Growth over year", datapoint: "+7.5%"}]}
							graphType="bar"
						/>

						<ZensorGraph
							chartName="Current desk utilization"
							value={100}
							comparisons={[
								{underline: "Growth over year", datapoint: "+7.5%"},
								{underline: "Growth over year", datapoint: "+7.5%"}]}
							graphType="gauge"
						/>
					</div>
				</div>
				</div>
			</div>

		);
	}

}

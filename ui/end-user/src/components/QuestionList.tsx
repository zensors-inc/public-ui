"use strict";

import "./QuestionList.scss";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import { navigate } from "../actions/navigation-actions";

import QuestionListTable from "./QuestionListTable";
import SearchFilter from "./SearchFilter";

import SensorListStore, { SensorListing } from "../stores/SensorListStore";

import { resetSensorFilter } from "../actions/sensor-actions";

type Props = {};

type State = {
	sensors?: SensorListing[];
	filteredSensors?: SensorListing[]
};

export default class QuestionList extends React.Component<{}, State> {
	public constructor(props: Props, context: any) {
		super(props, context);
		this.state = this.getState();
	}
	public componentDidMount() {
		SensorListStore.addSensorListUpdateListener(() => this.updateState());
	}

	public componentWillUnmount() {
		resetSensorFilter();
		SensorListStore.removeSensorListUpdateListener(() => this.updateState());
	}

	public getState(): State {
		return {
			sensors: SensorListStore.sensors,
			filteredSensors: SensorListStore.filteredSensors
		};
	}

	public updateState(): void {
		this.setState(this.getState());
	}

	public render() {
		return (
			<div className="container">
				<div className="page-subheading h400">Manage Questions</div>
				<div className="page-main-heading"> All Questions </div>
				<div className="page-action-label"
					onClick={() => navigate("/sensor/new") }>
					New question
				</div>

				<div className="question-list-grid">
					<SearchFilter />
					<QuestionListTable />
				</div>

			</div>
		);
	}

}

"use strict";

import "./HomeScreen.scss";

import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import { navigate } from "../actions/navigation-actions";

import { SensorType } from "@zensors/public-gql-client";

import ComparisonGrid from "./ComparisonGrid";
import QuestionListTable from "./QuestionListTable";
import SearchFilter from "./SearchFilter";

import DashboardStore from "../stores/DashboardStore";
import SensorListStore, { SensorListing } from "../stores/SensorListStore";
import { SensorDetails } from "../stores/SensorStore";
import UserStore from "../stores/UserStore";

import ZensorGraph from "./ZensorGraph";

type ActionTileProps = {
	header?: string;
	headerDetail?: number;
	label: string;
	icon?: string;
	destination: string;
};

class ActionTile extends React.Component<ActionTileProps, {}> {
	public render() {
		return (
			<div className="home-screen-action-tile">
				<div className="tile-header">
				{this.props.header
					? <span>{this.props.header}</span>
					: null
				}
				{this.props.headerDetail
					? <span> (<span
						className="header-detail"
						onClick={() => navigate(this.props.destination)}>
						{this.props.headerDetail}
						</span>)</span>
					: null
				}
				</div>
				<div className="tile"
					onClick={ () => navigate(this.props.destination) }>
					<div className="tile-label">
						{this.props.icon
						? <span className={`pt-icon-standard pt-icon-${this.props.icon}`}/>
						: null}
						&nbsp;&nbsp;{this.props.label}
					</div>
				</div>
			</div>
		);
	}
}

type Props = {};
type State = {
	sensors?: SensorDetails[];
	uids: string[];
	drawerVisible: boolean;
};

export default class HomeScreen extends React.Component<Props, State> {
	private drawer: HTMLDivElement | null;

	public constructor(props: Props, context: any) {
		super(props, context);
		this.state = {
			sensors: DashboardStore.visualizationSensors,
			uids: DashboardStore.uids,
			drawerVisible: false,
		};
		this.drawer = null;
	}

	public componentDidMount() {
		DashboardStore.addSensorListUpdateListener(() => this.updateState());
	}

	public componentWillUnmount() {
		DashboardStore.removeSensorListUpdateListener(() => this.updateState());
	}

	public getState(): State {
		return {
			sensors: DashboardStore.visualizationSensors,
			uids: DashboardStore.uids,
			drawerVisible: this.state.drawerVisible,
		};
	}

	public updateState(): void {
		this.setState(this.getState());
	}

	public handleClick(e: React.MouseEvent) {
		if (!this.drawer!.contains(e.target as HTMLElement)
			&& this.state.drawerVisible) {
			this.setState({drawerVisible: false});
		}
	}

	public render() {
		let firstName = UserStore.firstName;
		let lastName = UserStore.lastName;
		let sensors = SensorListStore.sensors;
		let activeSensorCount = sensors.filter((sensor) => sensor.active).length;
		let sensorVizs;

		if (this.state.sensors && this.state.sensors.length > 0) {
			// Leaving this here, assuming we get the data to make the dashboard from the sensor
			sensorVizs = this.state.sensors.map((sensor) =>
								<ZensorGraph
									chartName={sensor.name}
									// Timestamps should be normalized and parsed
									xSeries={[5, 2, 5, 2, 5, 2]}
									// Doesnt have any actual data yet, so just doing this with timestamps now
									ySeries={[0, 1, 2, 3, 4, 5]}
									graphType="line"
								/>
		);
		} else {
			sensorVizs = null;
		}

		let actions = [
			{
				header: "Active questions",
				headerDetail: activeSensorCount,
				label: "Manage questions",
				icon: IconNames.HELP,
				destination: "/sensor/questions",
			},
			{
				header: "Cameras",
				label: "Manage Cameras",
				icon: IconNames.CAMERA,
				destination: "/device",
			},
			{
				label: "Ask a new question",
				icon: IconNames.ADD,
				destination: "/sensor/new",
			},
		];
		let actionTileItems = actions.map( (action, i) =>
				<ActionTile key={i}
					header={action.header}
					headerDetail={action.headerDetail}
					label={action.label}
					icon={action.icon}
					destination={action.destination}
				/>
			);

		return (
			<div className="container" onClick={(e) => this.handleClick(e)}>
				<div className="page-subheading h400">
					Welcome, {firstName} {lastName}
				</div>
				<div className="page-main-heading">Coworking Co on Franklin Street</div>

				<div className="home-screen-grid">

					<div className="home-screen-actions">
						{actionTileItems}
					</div>

				<div className="home-screen-visualizations">
					<div className="tile-header">
						Live visualizations
						(<span className="header-detail"
							onClick={() => this.setState({drawerVisible: !this.state.drawerVisible})}>
							+
						</span>)
					</div>

					<div className={"zs-dash-drawer" + ((this.state.drawerVisible) ? "" : " zs-dash-drawer-closed")}
						ref={ (node) => this.drawer = node }>
						<div className="h500"> Add a new widget </div>
						<QuestionListTable/>
					</div>

					<div className="location-data-grid">
						<ComparisonGrid
							comparisons={[
								{datapoint: "7.5%", underline: "Year over year"},
								{datapoint: "7.5%", underline: "Year over year"},
								{datapoint: "7.5%", underline: "Year over year"},
								{datapoint: "7.5%", underline: "Year over year"}
							]}
						/>
						<ZensorGraph
							chartName="Historical growth over 4 months"
							xSeries={[1, 2, 3, 4, 5, 6]}
							ySeries={[1, 7, 8, 0, 4, 2]}
							comparisons={[
							{underline: "Growth over year", datapoint: "+7.5%"},
							{underline: "Growth over year", datapoint: "+7.5%"}]}
							graphType="line"
						/>

						<ZensorGraph
							chartName="Snapshot taken today"
							xSeries={[1, 2, 3, 4, 5, 6]}
							ySeries={[1, 7, 8, 0, 4, 2]}
							comparisons={[
								{underline: "Growth over year", datapoint: "+7.5%"},
								{underline: "Growth over year", datapoint: "+7.5%"}]}
							graphType="bar"
						/>

						<ZensorGraph
							chartName="Current desk utilization"
							value={100}
							comparisons={[
								{underline: "Growth over year", datapoint: "+7.5%"},
								{underline: "Growth over year", datapoint: "+7.5%"}]}
							graphType="gauge"
						/>

						{sensorVizs}
					</div>
				</div>

				</div>

			</div>
		);
	}
}

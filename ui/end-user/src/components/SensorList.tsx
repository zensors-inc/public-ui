"use strict";

import { Button, Classes, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import SensorListStore, { SensorListing } from "../stores/SensorListStore";

import { navigate } from "../actions/navigation-actions";

type Props = {

};

type State = {
	sensors?: SensorListing[];
};

export default class SensorList extends React.Component<Props, State> {
	public constructor(props: Props, context: any) {
		super(props, context);
		SensorListStore.addSensorListUpdateListener(() => this.updateState());
		this.state = this.getState();
	}

	public render() {
		if (this.state.sensors) {
			let sensors = this.state.sensors.map((sensor) =>
							<div
								className="sensor-listing"
								key={sensor.uid}
								onClick={() => navigate(`/sensor/${sensor.uid}`)}>
									{sensor.name} ({sensor.uid})
							</div>);


			return (
				<main id="sensor-list">
					<h2>Sensors</h2>
					<div className="sensor-list">
						{sensors}
					</div>
				</main>
			);
		} else {
			return (
				<main id="sensor-list">
					<h2>Sensors</h2>
					<div>Error loading sensors.</div>
				</main>
			);
		}
	}

	private getState(): State {
		return {
			sensors: SensorListStore.sensors
		};
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

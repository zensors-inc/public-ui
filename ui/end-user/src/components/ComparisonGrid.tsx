"use strict";

import ReactEcharts from "echarts-for-react";
import * as React from "react";

import "./DataViz.scss";

import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

type Props = {
	comparisons: {underline?: string, datapoint: string}[],
	// Should only allow 2,3,4xn - classname definition
	layout?: string,
};

// <Props, {}>
export default class ComparisonGrid extends React.Component<Props, {}> {
	public render() {
		let comparisons = this.props.comparisons.map((comparison, i) => {
			return (
			<div className="zs-comparison-field" key={i}>
				<div className="comparison-data">
					<h1>{comparison.datapoint}</h1>
					{(comparison.underline) ? <div className="comparison-underline"> {comparison.underline} </div> : null}
				</div>
			</div>
			);
		});
		// let chart = this.initiatlizeGraph();
		return (
			<div className="zs-graph-box">
				{/* Should be a prop */}
				<div className="zs-graph-header">
					<h1> Some kind of comparison</h1>
					<div className="zs-graph-settings">
						<Icon icon={IconNames.COG} iconSize={15}/>
					</div>
				</div>
				<div className="zs-data-comparison">
					{comparisons}
				</div>
			</div>
		);
	}
}


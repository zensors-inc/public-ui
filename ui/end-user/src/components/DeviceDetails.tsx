"use strict";

import { Button, Card, Classes, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import ClickToCopy from "./ClickToCopy";

import DeviceDetailsStore, { DeviceDetails as Device } from "../stores/DeviceStore";

import { loadDeviceDetails, setCropDisplay } from "../actions/device-actions";

import "./DeviceDetails.scss";

type Props = {
	uid: string;
};

type State = {
	device?: Device;
	size?: [number, number];
	crop?: [number, number][];
};

export default class DeviceDetails extends React.Component<Props, State> {
	private imgElement?: HTMLImageElement | null;
	private svgElement?: SVGSVGElement | null;

	public constructor(props: Props, context: any) {
		super(props, context);
		DeviceDetailsStore.addDeviceDetailsUpdateListener(() => this.updateState());

		if (DeviceDetailsStore.device && DeviceDetailsStore.device.uid === this.props.uid) {
			this.state = this.getState();
		} else {
			this.state = {};
			loadDeviceDetails(this.props.uid);
		}
	}

	public render() {
		if (this.state.device) {
			let polygonStr = this.state.crop ? this.state.crop.map(([x, y]) => `${x},${y}`).join(" ") : "";
			console.log(polygonStr);
			return (
				<main id="device-details">
					<h2>{this.state.device.name}</h2>
					<div className="main-container">
						<div className="device-image-area">
							{ this.state.device.data.length > 0
								? <img
									src={"//figzbuild.andrew.cmu.edu"
										+ this.state.device.data[this.state.device.data.length - 1].url}
									onLoad={() => this.onBackgroundLoad()}
									ref={(elt) => { this.imgElement = elt; }}
								/>
								: undefined }
							{ polygonStr && this.state.size
								?
									<svg
										className="polygon-input-selector"
										viewBox="0 0 1 1"
										preserveAspectRatio="none"
										width={this.state.size ? this.state.size[0] : 0}
										height={this.state.size ? this.state.size[1] : 0}
										ref={(elt) => this.svgElement = elt}
										style={{
											strokeWidth: 2 / this.state.size[1],
											strokeDasharray: `${8 / this.state.size[1]}`
										}}
									>
										<defs>
											<mask id="polygon">
												<rect className="mask-back" x={0} y={0} width={1} height={1} />
												<polygon className="mask-front" points={polygonStr} />
											</mask>
										</defs>
										<polygon points={polygonStr} />
										<rect className="darken" x={0} y={0} width={1} height={1} mask="url(#polygon)" />
									</svg>
								: undefined }
						</div>
						<div>
							<Card className="device-auth-list">
								<h4>Credentials</h4>
								{this.state.device.auth.map(this.renderCredentials)}
							</Card>
							<Card>
								<h3>Sensors</h3>
								<div className="device-sensor-list">
									{this.state.device.sensors.map((sensor) => {
										console.log(sensor);
										let cropArea = sensor.cropArea
											.match(/\(\d*(\.\d*)?,\d*(\.\d*)?\)/g)!
											.map((pt) => { console.log(pt); return pt; })
											.map((pt) => pt.substring(1, pt.length - 1))
											.map((pt) => pt.split(",").map((coord) => Number(coord)) as [number, number]);

										let dataPoint =
											sensor.data.length > 0
											? (sensor.sensorType === SensorType.COUNT ? sensor.data[0].label : (sensor.data[0].label ? "YES" : "NO"))
											: "NO DATA";

										return (
											<div
												className="sensor-listing"
												key={sensor.uid}
												onMouseEnter={() => setCropDisplay(cropArea)}
												onMouseLeave={() => setCropDisplay(undefined)}
											>
												<div className="sensor-name">{sensor.name}</div>
												<div className="sensor-latest-value">{dataPoint}</div>
											</div>
										);
									})}
								</div>
							</Card>
						</div>
					</div>
				</main>
			);
		} else {
			return (
				<main id="device-details">
					<h2>Error -- invalid device</h2>
				</main>
			);
		}
	}

	private renderCredentials(auth: any, i: number) {
		if (!auth) { return null; }

		let credentialsSection;

		if (auth.protocol === "FTP") {
			let username = auth.credentials.split(":")[0];
			let password = auth.credentials.split(":")[1];
			credentialsSection = (
				<div>
					<div>
						Username: <span className="mono"><ClickToCopy content={username} /></span>
					</div>
					<div>
						Password: <span className="mono"><ClickToCopy content={password} /></span>
					</div>
				</div>
			);
		} else {
			credentialsSection = (
				<div>
					Credentials: <span className="mono"><ClickToCopy content={auth.credentials} /></span>
				</div>
			);
		}

		return (
			<div className="auth-listing" key={i}>
				<div>
					Protocol: <span className="mono">{auth.protocol}</span>
				</div>
				{credentialsSection}
			</div>
		);
	}

	private getState(): State {
		return {
			device: DeviceDetailsStore.device,
			crop: DeviceDetailsStore.crop
		};
	}

	private onBackgroundLoad() {
		if (this.imgElement) {
			this.setState({
				size: [this.imgElement.width, this.imgElement.height]
			});
			this.forceUpdate();
		}
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

"use strict";

import "./PageElements.scss";

import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import UserStore from "../stores/UserStore";
import MenuItem from "./MenuItem";

import { navigate } from "../actions/navigation-actions";

type Props = {
	authenticated?: boolean;
};

type State = {
	expanded?: boolean;
};

// Can do lookup type to make sure it's part of props
// SAFE THIS FOR LATER IF WE NEED MORE DYNAMIC MENUS
// type NavigationItem = {
// 	href: // function
// }

// React component allows for type control on props and state (state is empty here, props is as defined)
// Props ARE passed in, state is NOT passed in
export default class FutureFooter extends React.Component<Props, {}> {
	public render() {
		if (this.props.authenticated !== undefined && this.props.authenticated) {
			return (
				<div id="zs-footer">
					<div id="zs-footer-top-container">
						<div className="zs-element-container">
								<div id="logo">
									<h1 className="logoType">ZENSORS</h1>
								</div>
							<div id="zs-footer-navigation">
								<div className="zs-footer-nav-block">
									<MenuItem
										linkText="About"
										onClick={() => navigate("/device")}
									/>
									<MenuItem
										linkText="Support"
										onClick={() => navigate("/device")}
									/>
									<MenuItem
										linkText="Contact"
										onClick={() => navigate("/device")}
									/>
								</div>
							</div>
						</div>
					</div>

					<div id="zs-footer-bottom">
						<div className="zs-element-container">
							<span className="zs-copyright-info">&copy;  Copyrighted by Zensors Inc.</span>
							<div id="zs-legal-nav">
								<MenuItem
									linkText="Privacy"
									onClick={() => navigate("/device")}
								/>
								<MenuItem
									linkText="Terms of service"
									onClick={() => navigate("/device")}
								/>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (
			<div></div>);
		}
	}

	private getState(): State {
		return {
			expanded: true
		};
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

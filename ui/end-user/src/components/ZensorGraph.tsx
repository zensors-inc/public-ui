"use strict";

import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import ReactEcharts from "echarts-for-react";
import * as React from "react";

import "./DataViz.scss";

type Props = {
	chartName?: string,
	// This should be an ENUM
	graphType: "bar" | "line" | "gauge" | "pie",
	xSeries?: number[],
	ySeries?: number[],
	value?: number,
	styleClass?: string,
	underline?: string,
	comparisons?: {underline?: string, datapoint: string}[],
};

export default class ZensorGraph extends React.Component<Props, {}> {
	public render() {

		// Creates the comparison blocks that may be displayed below the dataviz
		let comparisons;
		if (this.props.comparisons) {
			comparisons = this.props.comparisons.map((comparison, i) => {
				return (
				<div className="zs-comparison-field" key={i}>
					<div className="comparison-data">
						<h1>{comparison.datapoint}</h1>
						{(comparison.underline) ? <div className="comparison-underline"> {comparison.underline} </div> : null}
					</div>
				</div>
				);
			});
		}

		return (
			<div className="zs-graph-box">
				{/* Should be a prop */}
				<div className="zs-graph-header ">
					<h1> {this.props.chartName} </h1>
					<div className="zs-graph-settings">
						<Icon icon={IconNames.COG} iconSize={15}/>
					</div>
				</div>
				<ReactEcharts
				/* Maybe also pass in all/some of the options as prop? */
				option={this.getOption()}
				lazyUpdate={true}
				/>
				{(comparisons) ?
					<div className="zs-data-comparison">
						{comparisons}
					</div>
				:
				null
				}

			</div>
		);
	}
	private configureGraph() {
		let config = false;
		return config;
	}
	// $support-03: #00b583;  // green
	// $support-04: #6fa9ff;  // light blue
	// $support-05: #6470e7;
	private getOption() {
		let option: {};
		option = {
			tooltip: {},
			color: "#6470e7",
			xAxis: {
				data: this.props.xSeries,
				show: false
			},
			legend: {show: false},
			yAxis: {},
			height: 100,
			series: [{
				name: this.props.chartName,
				type: this.props.graphType,
				smooth: true,
				data: this.props.ySeries
			}]
		};

		if (this.props.graphType === "line" || this.props.graphType === "bar") {
			return option;
		}
		// Gauge chart...
		// use object assign to add configs
		option = {
			backgroundColor: "#ffffff",
			title: {
				text: `${this.props.value}%`,
				subtext: (this.props.underline) ? this.props.underline : false,
				x: "50%",
				y: "40%",
				textAlign: "center",
				textStyle: {
					fontWeight: "normal",
					fontSize: 32
				},
				subtextStyle: {
					fontWeight: "normal",
					fontSize: 12,
					color: "#BBBBBB"
				}
			},
			color: ["#6470e7"],
			series: {
				type: "pie",
				radius: ["50%", "70%"],
				avoidLabelOverlap: false,
				startAngle: 225,
				color: ["#6470e7", "transparent"],
				hoverAnimation: false,
				legendHoverLink: false,
				label: {
					normal: {
						show: false,
						position: "center"
					},
				},
				labelLine: {
					normal: {
						show: false
					}
				},
				data: [{
					// Max value is 75% to leave some space at the bottom yea
					value: 100,
					name: "1"
				}, {
					value: 30,
					name: "2"
				}]
			}
		};

		return option;
	}
}


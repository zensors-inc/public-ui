"use strict";

import "./CreateSensor.scss";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import { navigate } from "../actions/navigation-actions";
import { createSensor, refreshSensorList } from "../actions/sensor-actions";

import {
	createSensorCropAreaUpdate,
	createSensorDeviceUpdate,
	createSensorFrequencyUpdate,
	createSensorNameUpdate,
	createSensorQuestionUpdate,
	createSensorReset,
	createSensorTypeUpdate,
	keepCreateSensorFormData,
} from "../actions/create-sensor-actions";

import CreateSensorStore, {CreateSensorDetails} from "../stores/CreateSensorStore";
import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";

import {
	Button,
	ButtonStyle,
	Form,
	RadioButton,
	Submit,
	TextInput
} from "@zensors/core-ui";

import PolygonOverlay from "./PolygonOverlay";

type State = {
	form: CreateSensorDetails;
	sampleAnswer: string | undefined;
};

export default class SensorConfirmation extends React.Component<{}, State> {
	public constructor(props: {}, context: any) {
		super(props, context);
		this.state = this.getState();
	}

	public getState() {
		return {
			form: CreateSensorStore.sensorForm,
			sampleAnswer: undefined,
		};
	}

	public render() {

		let selectedDevice = DeviceListStore.devices.filter(({ uid }) => uid === this.state.form!.deviceUid)[0];
		let backgroundUrl = selectedDevice && selectedDevice.latest
			? ("//figzbuild.andrew.cmu.edu" + selectedDevice.latest.url)
			: undefined;

		return (
			<div className="container">

				<div className="page-subheading h400">New Question</div>
				<div className="page-main-heading"> Review and confirm </div>
				<div className="section-description">
					You’re almost ready to start gathering data for your question.
					Just provide a sample answer and review the details below,
					then you can either start running this question on a single
					camera or add it to additional cameras.
				</div>


				<div className="question-confirmation-grid">

					<div className="sample-answer">
						<div className="heading">
							Sample answer
						</div>

						<div className="image-display">
							<div className="label">
								Selected region
							</div>

							<PolygonOverlay
								url={backgroundUrl}
								crop={this.state.form.cropArea}
							/>
							<div className="timestamp">
								{(selectedDevice && selectedDevice.latest) ?
								new Date(selectedDevice.latest.timestamp).toString() :
								null}
							</div>
						</div>

						<div className="sample-answer-input">
							<div className="label">
								Question
							</div>

							<div className="content">
								{this.state.form.question}
							</div>

							{this.renderSampleAnswerInput(this.state.form.sensorType)}
						</div>
					</div>

					<div className="question-details">
						<div className="heading">
							Question details
						</div>

						<div className="col-first">
							<div className="label">
								Camera
							</div>

							<div className="content">
								{selectedDevice.name}
							</div>
						</div>

						<div className="col-second">
							<div className="label">
								Data type
							</div>

							<div className="content">
								{this.renderQuestionType(this.state.form.sensorType)}
							</div>
						</div>

						<div className="col-third">
							<div className="label">
								Sample rate
							</div>

							<div className="content">
								{this.renderSampleRate(this.state.form.frequency)}
							</div>
						</div>
					</div>

				{/*
				<div className="question-type">
					<div className="section-heading">Sensor details</div>
					<div className="subheading">Sensor name</div>
					{this.state.form.name}
					<div className="subheading">Device UID</div>
					{this.state.form.deviceUid}
				</div>


				<div className="question-input">
					<div className="section-heading">
						Question settings
					</div>

					<div className="section-description">
						<div className="subheading">Sensor name</div>
						{this.state.form.question}
					</div>

					<div className="section-description">
						<div className="subheading">Answer type</div>
						{this.state.form.sensorType}
						<div className="subheading">Check frequency</div>
						{this.state.form.frequency}
					</div>

				</div>

				<div className="sample-rate">
					<div className="section-heading">
						Sensor configuration
					</div>

					<div className="polygon-input">

						<PolygonOverlay
							url={backgroundUrl}
							crop={this.state.form.cropArea}
						/>
					</div>

				</div> */}

				</div>

				<div className="review-and-confirm-button button-container">
					<Button label="Edit sensor"
						onClick={() => this.modifySensor()}
						/>
					<Button label="Create Sensor"
						disabled={!this.state.form.valid}
						onClick={() => this.submitCreateSensor()}
						/>
				</div>
			</div>
		);
	}

	private renderSampleAnswerInput(sensorType: SensorType | undefined) {
		let handleChange = (val?: string) => this.setState({sampleAnswer: val});

		switch (sensorType) {
			case SensorType.COUNT:
				return (
					<div style={{marginTop: "20px"}}>
						<TextInput onChange={handleChange}
							data={this.state.sampleAnswer}
							placeholder="Please enter a number"/>
					</div>
				);

			case SensorType.YES_NO:
				return (
					<RadioButton<{}, string>
						options={[
								{ key: "YES", value: "yes" },
								{ key: "NO", value: "no" }
							]}
						onChange={handleChange}
						data={this.state.sampleAnswer}
					/>
				);

			default:
				return null;
		}
	}

	private renderQuestionType(sensorType: SensorType|undefined): string {
		switch (sensorType) {
			case SensorType.COUNT:
				return "Count";

			case SensorType.YES_NO:
				return "Yes or no";

			default:
				return "Error";
		}
	}

	private renderSampleRate(frequency: number|undefined): string {
		switch (frequency) {
			case 60:
				return "1 minute";

			case 300:
				return "5 minutes";

			case 3600:
				return "1 hour";

			case 86400:
				return "1 day";

			default:
				return frequency + " seconds";
		}
	}

	private modifySensor() {
		keepCreateSensorFormData(true);
		navigate("/sensorui/new");
	}

	private submitCreateSensor() {
		if (!this.state.form.valid) {
			return;
		}

		let data = {
			name: this.state.form.name,
			deviceUid: this.state.form.deviceUid!,
			question: this.state.form.question,
			frequency: this.state.form.frequency!,
			sensorType: this.state.form.sensorType!,
			cropArea: this.state.form.cropArea,
		};

		createSensor(data).then(() => {
			refreshSensorList();
			createSensorReset();
			navigate("/sensor");
		});
	}
}

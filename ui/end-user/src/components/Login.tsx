"use strict";

import { Button, Classes, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import { login, register } from "../actions/user-actions";

import { Form, Submit, TextInput } from "@zensors/core-ui";

type LoginForm = {
	email: string,
	password: string
};

type RegisterForm = {
	firstName: string,
	lastName: string,
	email: string,
	password: string,
	confirmPassword: string
};

type Props = {
	showRegisterForm: boolean
};

export default class Login extends React.Component<Props, {}> {
	public render() {
		let registerForm =
			this.props.showRegisterForm
				? (
					<Form<RegisterForm> submit={register} init={{
						firstName: "",
						lastName: "",
						email: "",
						password: "",
						confirmPassword: ""
					}}>
						<h1>Register</h1>
						<TextInput<RegisterForm>
							placeholder="First Name"
							getFormData={(data) => data.firstName}
							setFormData={(data, firstName) => data.firstName = firstName}
						/>
						<TextInput<RegisterForm>
							placeholder="Last Name"
							getFormData={(data) => data.lastName}
							setFormData={(data, lastName) => data.lastName = lastName}
						/>
						<TextInput<RegisterForm>
							placeholder="Email"
							getFormData={(data) => data.email}
							setFormData={(data, email) => data.email = email}
						/>
						<TextInput<RegisterForm>
							placeholder="Password"
							type="password"
							getFormData={(data) => data.password}
							setFormData={(data, password) => data.password = password}
						/>
						<TextInput<RegisterForm>
							placeholder="Confirm Password"
							type="password"
							getFormData={(data) => data.confirmPassword}
							setFormData={(data, confirmPassword) => data.confirmPassword = confirmPassword}
						/>
						<Submit rightIcon={IconNames.ARROW_RIGHT} text="Submit" />
					</Form>
				)
				: undefined;
		return (
			<div>
				<Form<LoginForm> submit={login} init={{ email: "", password: "" }}>
					<h1>Login</h1>
					<TextInput<LoginForm>
						placeholder="User Email"
						getFormData={(data) => data.email}
						setFormData={(data, email) => data.email = email}
					/>
					<TextInput<LoginForm>
						placeholder="Password"
						type="password"
						getFormData={(data) => data.password}
						setFormData={(data, password) => data.password = password}
					/>
					<Submit rightIcon={IconNames.ARROW_RIGHT} text="Submit" />
				</Form>
				{registerForm}
			</div>
		);
	}
}

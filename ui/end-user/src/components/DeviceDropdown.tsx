"use strict";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import {
	Input,
	inputSymbol,
	TextInput
} from "@zensors/core-ui";

import CreateSensorStore, {CreateSensorDetails} from "../stores/CreateSensorStore";
import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";

import { createSensorDeviceFilter} from "../actions/create-sensor-actions";

import "./DeviceDropdown.scss";

type Props<V> = {
	label?: string;
	options: { key: string, value: V }[];
};


// [Marvin] The only reason I'm doing all of this here is because otherwise we can't access this.state.dropdownOpen
// LMK if you know a cleaner fix for this
type S = {
	dropdownOpen: boolean,
	devices: DeviceListing[],
	selected: string,
	filteredDevices: DeviceListing[]
};

export default class DeviceDropdown<T, V> extends Input<T, V, V, Props<V>, S> {
	public static [inputSymbol] = true;
	public constructor(props: Props<V>, context: any) {
		super(props, context);
		createSensorDeviceFilter("");
		this.state = this.getState();
	}
	public getState() {
		return {
			devices: DeviceListStore.devices,
			selected: (CreateSensorStore.sensorForm.deviceUid) ?
			// Rewrite this with find so we don't need to acces index 0
			DeviceListStore.devices.filter((a) =>  a.uid === CreateSensorStore.sensorForm.deviceUid)[0].name
			: "No camera selected",
			filteredDevices: DeviceListStore.filteredDevices,
			dropdownOpen: false
		};
	}
	public updateState() {
		// It's only updating whatever it gets from getState and leaves the rest alone
		this.setState(this.getState());
	}
	public render() {
		let items = this.props.options.map(({ key, value }, i) => {
			return (
				<div key={i} onClick={() => this.handleDropdownSelection(i)}>
					<label>{key}</label>
				</div>
			);
		});
		let selectedItem = this.props.options[0];
		return (
			<div id="zs-dropdown">
				<div className="zs-dropdown-selected-item">{this.getState().selected}</div>
				<TextInput
					placeholder="Search for sensors"
					// Right now it's not typechecking this onChange?
					// Need to connect this to a flux action or nah?
					onChange={(value) => {
						createSensorDeviceFilter(value);
					}}

					// This is a really ugly hack, instead of setting true/false, use !this.state.dropdownOpen to
					// toggle more flexibly?
					onFocus={() => this.setState({dropdownOpen: true})}

				/>
			{(this.state.dropdownOpen !== false) ?
				<div id="zs-dropdown-results">
					{items}
				</div>
				: undefined}
			</div>
		);
	}

	private handleDropdownSelection = (i: number) => {
		if (this.props.onChange) {
			this.setState({dropdownOpen: false});
			// onChange={createSensorDeviceUpdate} (flux action from parent)
			this.props.onChange(this.props.options[i].value);
		}
	}
}

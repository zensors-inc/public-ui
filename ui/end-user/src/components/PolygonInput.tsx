"use strict";

import * as React from "react";

import { Input, InputProps, inputSymbol } from "@zensors/core-ui";

import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";

import "./PolygonInput.scss";

type Data = {
	deviceUid?: string;
	area: [number, number][];
};

type Props = {
	data?: Data;
};

type State = {
	size?: [number, number];
	hoverPosition?: [number, number];
	svgElement?: SVGSVGElement | null;
	imgElement?: HTMLImageElement | null;

};

export default class PolygonInput<T>
		extends Input<T, Data, [number, number][], Props, State> {
	public static [inputSymbol] = true;

	public constructor(props: Props, context: any) {
		super(props, context);
		this.state = {};
	}

	public shouldComponentUpdate(nextProps: Props, nextState: State) {
		return this.props.data !== nextProps.data
		|| this.state.hoverPosition !== nextState.hoverPosition;
		// TODO(mdsavage): we may want to not make this rely on DeviceListStore
	}
	public render() {
		if (this.props.data) {
			let polygonStr = this.props.data.area.map(([x, y]) => `${x},${y}`).join(" ");
			let hoverPoint;

			if (this.state.hoverPosition && this.state.size) {
				polygonStr += ` ${this.state.hoverPosition[0]},${this.state.hoverPosition[1]}`;
				hoverPoint =
					<ellipse
						className="hover"
						cx={this.state.hoverPosition[0]}
						cy={this.state.hoverPosition[1]}
						rx={5 / this.state.size[0]}
						ry={5 / this.state.size[1]}
					/>;
			}


			let selectedDevice = DeviceListStore.devices.filter(({ uid }) => uid === this.props.data!.deviceUid)[0];
			let background = selectedDevice && selectedDevice.latest
				? ("//figzbuild.andrew.cmu.edu" + selectedDevice.latest.url)
				: undefined;
			let svg =
				this.state.size
					? (
						<svg
							className="polygon-input-selector"
							onClick={(e) => this.onSvgClick(e)}
							onMouseMove={(e) => this.onMouseMove(e)}
							onMouseLeave={(e) => this.onMouseLeave(e)}
							viewBox="0 0 1 1"
							preserveAspectRatio="none"
							width={this.state.size ? this.state.size[0] : 0}
							height={this.state.size ? this.state.size[1] : 0}
							ref={(elt) => this.setState({ svgElement: elt })}
							style={{
								strokeWidth: 2 / this.state.size[1],
								// strokeDasharray: `${8 / this.state.size[1]}`
							}}
						>
							<defs>
								<mask id="polygon">
									<rect className="mask-back" x={0} y={0} width={1} height={1} />
									<polygon className="mask-front" points={polygonStr} />
								</mask>
							</defs>
							<polygon points={polygonStr} />
							<rect className="darken" x={0} y={0} width={1} height={1} mask="url(#polygon)" />
							{this.props.data.area.map(([x, y], i) =>
								<ellipse key={i} cx={x} cy={y} rx={5 / this.state.size![0]} ry={5 / this.state.size![1]} />)}
							{hoverPoint}
						</svg>
					)
					: undefined;

			return (
				<div className="polygon-input">
					<div id="zs-timestamp">{(selectedDevice && selectedDevice.latest) ?
						new Date(selectedDevice.latest.timestamp).toString() :
						"No camera selected"}
					</div>
					<img
						src={background}
						onLoad={() => this.onBackgroundLoad()}
						ref={(elt) => { this.setState({ imgElement: elt }); }}
					/>
					{svg}
				</div>
			);
		} else { // TODO: What to do here?
			throw new Error("fail");
		}
	}

	private onBackgroundLoad() {
		if (this.state.imgElement) {
			this.setState({
				size: [this.state.imgElement.width, this.state.imgElement.height]
			});
			this.forceUpdate();
		}
	}

	private onSvgClick(e: React.MouseEvent<SVGSVGElement>) {
		if (this.state.size && this.props.onChange && this.props.data) {
			let update = this.props.data.area.slice();
			update.push([ e.nativeEvent.offsetX / this.state.size[0], e.nativeEvent.offsetY / this.state.size[1] ]);
			this.props.onChange(update);
		}
	}

	private onMouseMove(e: React.MouseEvent<SVGSVGElement>) {
		if (this.state.size) {
			this.setState({
				hoverPosition: [ e.nativeEvent.offsetX / this.state.size[0], e.nativeEvent.offsetY / this.state.size[1] ]
			});
		}
	}

	private onMouseLeave(e: React.MouseEvent<SVGSVGElement>) {
		this.setState({ hoverPosition: undefined });
	}
}

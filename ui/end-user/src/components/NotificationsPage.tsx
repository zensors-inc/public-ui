"use strict";

import { IconNames } from "@blueprintjs/icons";

import "./NotificationsPage.scss";

import * as React from "react";

import { TextInput } from "@zensors/core-ui";

type Notification = {
	question: string,
	camera: string,
	location: string,
	timestamp: string,
	status: string,
};

type TileProps = {
	notification: Notification,
};
type TileState = {};

class NotificationTile extends React.Component<TileProps, TileState> {

	public render() {
		return (
			<div className="notification-tile">
				<div className="notification-tile-question">
					{this.props.notification.question}
				</div>

				<div className="notification-tile-location">
					{this.props.notification.location}
				</div>

				<div  className="notification-tile-camera">
					{this.props.notification.camera}
				</div>

				<div className="notification-tile-status">
					{this.props.notification.status}
				</div>

				<div className="notification-tile-timestamp">
					{this.props.notification.timestamp}
				</div>
			</div>
		);
	}
}

type Props = {};
type State = {};

export default class NotificationsPage extends React.Component<Props, State> {

	public constructor(props: Props, context: any) {
		super(props, context);
	}

	public render() {
		let newNotifications = [
			{
				question: "Is the trash can full?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "3:21 PM",
				status: "Yes",
			},
			{
				question: "Is the projector screen down?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "12:05 PM",
				status: "Yes",
			},
			{
				question: "Is the trash can full?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "11:32 AM",
				status: "Yes",
			},
			{
				question: "Is the trash can full?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "06/12",
				status: "Yes",
			},
		];
		let recentNotifications = [
			{
				question: "Is the trash can full?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "06/11",
				status: "Yes",
			},
			{
				question: "Is the projector screen down?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "06/08",
				status: "No",
			},
			{
				question: "Is the trash can full?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "06/06",
				status: "No",
			},
			{
				question: "Is the trash can full?",
				camera: "Camera 2",
				location: "Floor 2, Butler Street",
				timestamp: "06/03",
				status: "No",
			},
		];

		let newNotificationItems = newNotifications.map((val, i) => {
			return (
				<NotificationTile key={i} notification={val}/>
			);
		});

		let recentNotificationItems = recentNotifications.map((val, i) => {
			return (
				<NotificationTile key={i} notification={val}/>
			);
		});

		return (
			<div className="container">
				<div className="page-subheading h400">Notifications</div>
				<div className="page-main-heading"> All Notifications (DEMO) </div>
				<div className="page-action-label">
					Archive new notifications
				</div>

				<div className="section-description">
					To help figure out what the right sample rate is,
					review tips to the right.
				</div>

				<div className="notification-page-content">
					<div className="notification-search-text-input">
						<TextInput placeholder="Search keywords"/>
					</div>

					<div className="new-notification-section">
						<div className="section-heading">
							{newNotifications.length} New notificatons
						</div>
						{newNotificationItems}
					</div>

					<div className="recent-notification-section">
						<div className="section-heading">
							Recent Notifications
						</div>
						{recentNotificationItems}
					</div>

				</div>

			</div>
		);
	}
}

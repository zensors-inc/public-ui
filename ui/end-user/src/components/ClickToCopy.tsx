"use strict";

import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import "./ClickToCopy.scss";

type Props = {
	content: string;
};

type State = {};

export default class ClickToCopy extends React.Component<Props, State> {
	private selectableContent?: Element | null;

	// constructor(props, context) {
	// 	super(props, context);
	// 	this.selectableContent = React.createRef();
	// }

	public render() {
		return (
			<span ref={(elt) => this.selectableContent = elt}>
				{this.props.content}
				<span className="copy" onClick={() => this.copy()}>
					<Icon icon={IconNames.DUPLICATE} iconSize={12} />
				</span>
			</span>
		);
	}

	private copy() {
		if (!this.selectableContent) {
			return;
		}

		let range = document.createRange();
		range.selectNode(this.selectableContent);

		let selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);

		document.execCommand("copy");

		selection.removeAllRanges();
	}
}

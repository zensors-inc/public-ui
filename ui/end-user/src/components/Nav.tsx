"use strict";

import "./PageElements.scss";

import { Icon } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import UserStore from "../stores/UserStore";
import MenuItem from "./MenuItem";


import { navigate } from "../actions/navigation-actions";

type Props = {
	authenticated?: boolean;
};

type State = {
	expanded?: boolean;
	foldOutVisible?: boolean
};

// Can do lookup type to make sure it's part of props
// SAFE THIS FOR LATER IF WE NEED MORE DYNAMIC MENUS
// type NavigationItem = {
// 	href: // function
// }

// React component allows for type control on props and state (state is empty here, props is as defined)
// Props ARE passed in, state is NOT passed in
export default class FutureNav extends React.Component<Props, State> {
	constructor(props: Props, context: any) {
		super(props, context);
		this.state = {
			foldOutVisible: false
		};
	}

	public render() {
		let menuVisibilityClass = this.state.foldOutVisible ? "" : "hidden";
		if (this.props.authenticated !== undefined && this.props.authenticated) {
			return (
				<div id="zs-menu">
					<div className="zs-element-container">
						<div id="zs-menu-top-container">
							<div id="logo">
								<h1 className="logoType"
									style={{cursor: "pointer"}}
									onClick={() => navigate("/home")}>
									ZENSORS
								</h1>
							</div>
							<div id="zs-burger-menu" onClick={this.toggleFoldOutMenu.bind(this)}>
								<Icon icon="menu" iconSize={20}/>
							</div>
						</div>
						<div id="zs-menu-navigation" className={menuVisibilityClass}>
							<div className="zs-nav-block">

								<MenuItem
									linkText="Home"
									onClick={() => navigate("/home")}
								/>

								<MenuItem
									linkText="Questions"
									onClick={() => navigate("/sensor/questions")}
								/>

								<MenuItem
									linkText="Ask a Question"
									onClick={() => navigate("/sensor/new")}
								/>

							</div>
							<div className="zs-control-block">
								<MenuItem
									leftDivider={true}
									// IconNames is an ENUM
									icon={IconNames.COG}
									linkText="Settings"
									onClick={() => navigate("/device")}
								/>

								{/*
								<MenuItem
									// IconNames is an ENUM
									icon={IconNames.NOTIFICATIONS}
									linkText="Notifications"
									onClick={() => navigate("/notifications")}
								/>
								*/}

								<MenuItem
									// IconNames is an ENUM
									icon={IconNames.LOG_OUT}
									linkText="Log out"
									onClick={() => navigate("/")}
								/>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (
			<div></div>);
		}
	}

	private toggleFoldOutMenu(): void {
		this.setState({foldOutVisible: !this.state.foldOutVisible});
	}
	private getState(): State {
		return {
			expanded: true
		};
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

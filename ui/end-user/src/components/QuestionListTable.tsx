"use strict";

import "./QuestionList.scss";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import { navigate } from "../actions/navigation-actions";

import SensorListStore, { SensorListing } from "../stores/SensorListStore";

import { Checkbox } from "@zensors/core-ui";

import { toggleVisualization } from "../actions/dashboard-actions";

const QUESTION_LENGTH_LIMIT = 35;

type TableRowProps = {
	sensor: SensorListing;
};

type TableRowState = {};

class QuestionListTableRow extends React.Component<TableRowProps, TableRowState> {

	public render() {
		return (
			<div className="sensor-listing">
				<div>
					<Checkbox onChange={() => toggleVisualization(this.props.sensor.uid)}/>
				</div>

				<div>
					<span className="question-name-label"
						onClick={() => navigate(`/sensor/${this.props.sensor.uid}`)}>
						{this.renderQuestionBody(this.props.sensor.question)}
					</span>
				</div>

				<div>
					{this.props.sensor.active ? "Live" : "Error"}
				</div>

				<div>
					{this.props.sensor.device.name}
				</div>

				<div>
					{this.renderQuestionType(this.props.sensor.sensorType)}
				</div>

				<div>
					1/12/18
				</div>
			</div>
		);
	}

	private renderQuestionBody(question: string) {
		if (question.length > QUESTION_LENGTH_LIMIT) {
			question = question.slice(0, QUESTION_LENGTH_LIMIT) + "...";
		}
		return question + "	>";
	}

	private renderQuestionType(sensorType: SensorType): string {
		switch (sensorType) {
			case SensorType.COUNT:
				return "Count";

			case SensorType.YES_NO:
				return "Yes or no";

			default:
				return "Error";
		}
	}
}


type Props = {};

type State = {
	sensors?: SensorListing[]
};

export default class QuestionListTable extends React.Component<Props, State> {
	public constructor(props: Props, context: any) {
		super(props, context);
		this.state = this.getState();
	}

	public componentDidMount() {
		SensorListStore.addSensorListUpdateListener(() => this.updateState());
	}

	public componentWillUnmount() {
		SensorListStore.removeSensorListUpdateListener(() => this.updateState());
	}

	public getState(): State {
		return {
			sensors: SensorListStore.filteredSensors
		};
	}

	public updateState() {
		this.setState(this.getState());
	}

	public render() {
		let sensors;

		let columnLabels = (
			<div className="question-list-column-labels">
				<div></div>
				<div>QUESTIONS</div>
				<div>STATUS</div>
				<div>CAMERA</div>
				<div>TYPE</div>
				<div>STARTED</div>
			</div>);

		if (this.state.sensors) {
			sensors = this.state.sensors.map((sensor, i) =>
				<QuestionListTableRow sensor={sensor} key={i} />
				);
		} else {
			sensors = (<div><div/><div className="h300">None</div></div>);
		}

		return (
			<div className="question-list-table">

				{columnLabels}

				{sensors}

			</div>
		);
	}
}

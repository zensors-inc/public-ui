"use strict";

import { Button, Classes, InputGroup } from "@blueprintjs/core";

import * as React from "react";

import "./App.scss";

import {
	Link,
	Redirect,
	Route,
	Router,
	Switch,
} from "react-router-dom";

import CameraDetails from "./CameraDetails";
import CreateDevice from "./CreateDevice";
import CreateSensor from "./CreateSensor";
import Dashboard from "./Dashboard";
import DeviceDetails from "./DeviceDetails";
import DeviceList from "./DeviceList";
import Footer from "./Footer";
import HomeScreen from "./HomeScreen";
import Login from "./Login";
import Nav from "./Nav";
import NotificationsPage from "./NotificationsPage";
import QuestionDetails from "./QuestionDetails";
import QuestionList from "./QuestionList";
import SensorConfirmation from "./SensorConfirmation";
import SensorList from "./SensorList";

import UserStore from "../stores/UserStore";

import { history, navigate } from "../actions/navigation-actions";
import { loadUserData } from "../actions/user-actions";

import { switchCSSTransition } from "./SwitchCSSTransitionWrapper";

type State = {
	authenticated?: boolean;
	userFirstName?: string;
	userLastName?: string;
};

function init() {
	loadUserData();
}

const CSSTransitionSwitch = switchCSSTransition(Switch, "fade", history);

export default class App extends React.Component<any, State> {
	public constructor(props: {}, context: any) {
		super(props, context);
		this.state = {};
		UserStore.addUserDataLoadListener(() => this.onUserDataLoad());
		init();
	}

	public render() {
		let main;

		if (this.state.authenticated === undefined) {
			main = "Loading...";
		} else {
			main = (
				<CSSTransitionSwitch>
					<Route exact={true} path="/" component={() => <Login showRegisterForm={false} />}/>
					<Route exact={true} path="/device" component={DeviceList}/>
					<Route exact={true} path="/dashboard" component={Dashboard}/>
					<Route exact={true} path="/device/new" component={CreateDevice}/>
					<Route
						exact={true}
						path="/device/:uid"
						component={({ match }: { match: { params: { uid: string } } }) =>
							<CameraDetails uid={match.params.uid} />
						}
					/>
					<Route exact={true} path="/sensor" component={QuestionList}/>
					<Route exact={true} path="/sensor/new" component={CreateSensor}/>
					<Route exact={true} path="/sensor/questions" component={QuestionList}/>
					<Route exact={true} path="/sensor/confirmation" component={SensorConfirmation}/>
					<Route
						exact={true}
						path="/sensor/:uid"
						component={({ match }: { match: { params: { uid: string } } }) =>
							<QuestionDetails uid={match.params.uid} />
						}
					/>
					<Route exact={true} path="/notifications" component={NotificationsPage}/>
					<Route exact={true} path="/home" component={HomeScreen}/>
				</CSSTransitionSwitch>
			);
		}

		return (
			<Router history={history}>
			<div id="page-container">
				<Nav authenticated={this.state.authenticated} />
					<div className="root-container" id="end-user">
						{main}
					</div>
				<Footer authenticated={this.state.authenticated} />
			</div>
			</Router>
		);
	}

	private onUserDataLoad() {
		if (UserStore.uid !== undefined) {
			this.setState({
				authenticated: true,
				userFirstName: UserStore.firstName,
				userLastName: UserStore.lastName
			});
			navigate("/home");
		} else {
			this.setState({
				authenticated: false
			});
		}
	}
}

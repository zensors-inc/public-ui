import * as React from "react";

import { History } from "history";

import { CSSTransition, TransitionGroup } from "react-transition-group";

export const switchCSSTransition =
	(Switch: React.ComponentType<any>, transitionClass: string, history: History) =>
	class CSSTransitionSwitch extends React.Component<any> {
		public render() {
			return (
				<TransitionGroup>
					<CSSTransition
						key={history.location.key}
						classNames={transitionClass}
						timeout={{enter: 300, exit: 0}}>
						<Switch location={history.location} {...this.props}/>
					</CSSTransition>
				</TransitionGroup>
			);
		}
	};

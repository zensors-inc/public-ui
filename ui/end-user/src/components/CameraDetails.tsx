"use strict";

import { Button, Card, Classes, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import { navigate } from "../actions/navigation-actions";

import ClickToCopy from "./ClickToCopy";

import DeviceDetailsStore, { DeviceDetails as Device } from "../stores/DeviceStore";

import { loadDeviceDetails } from "../actions/device-actions";

import "./CameraDetails.scss";

type Props = {
	uid: string;
};

type State = {
	device?: Device;
	size?: [number, number];
};

export default class CameraDetails extends React.Component<Props, State> {
	private imgElement?: HTMLImageElement | null;
	private svgElement?: SVGSVGElement | null;
	private dummyCamera: Device;

	public constructor(props: Props, context: any) {
		super(props, context);
		DeviceDetailsStore.addDeviceDetailsUpdateListener(() => this.updateState());

		this.dummyCamera = {
			uid: "yo_mama",
			name: "My Dummy Camera",
			auth: [{credentials: "123:456", protocol: "FTP"}],
			data: [],
			sensors: [
				{
					uid: "123",
					name: "my_question_1",
					cropArea: "null",
					sensorType: SensorType.YES_NO,
					data: [],
				},
				{
					uid: "456",
					name: "my_question_2",
					cropArea: "null",
					sensorType: SensorType.YES_NO,
					data: [],
				}
			]
		};

		this.state = this.getState();

		if (DeviceDetailsStore.device && DeviceDetailsStore.device.uid === this.props.uid) {
			this.state = this.getState();
		} else {
			this.state = {};
			loadDeviceDetails(this.props.uid);
		}
	}

	public render() {
		if (this.state.device) {
			return (
				<div className="container">

					<div className="page-subheading h400">
						<span className="navigation-label"
							onClick={ () => navigate("/device") }>
							Manage Cameras
						</span>

						<span> > Camera details</span>
					</div>

					<div className="page-action-label"
						onClick={() => navigate("/sensor/new") }>
						New question
					</div>

					<div className="camera-details-grid">
						<div className="camera-details-image">
							{ this.state.device.data.length > 0
								? <img
									src={"//figzbuild.andrew.cmu.edu"
										+ this.state.device.data[this.state.device.data.length - 1].url}
									onLoad={() => this.onBackgroundLoad()}
									ref={(elt) => { this.imgElement = elt; }}
								/>
								: <div className="h400">Image not available</div> }
						</div>

						<div className="camera-details-credentials">
							<div className="label"> Network Credentials </div>
							{this.state.device.auth.map(this.renderCredentials)}
						</div>

						<div className="camera-details-questions">
							<div className="heading">
								Questions ({this.state.device.sensors.length})
							</div>

							<div className="subheading">
								This is an overview of questions running on this camera.
							</div>

							<div className="question-row column-label">
								<div>
									Question
								</div>

								<div>
									Data
								</div>
							</div>

							{this.state.device.sensors.length > 0
								? this.state.device.sensors.map(((sensor, i) =>
									this.renderQuestionRow(sensor.uid, sensor.name, this.renderQuestionData(sensor))))
								: <div style={{marginTop: "15px"}}> No questions on this camera yet </div>}
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div className="container">
					<div className="page-subheading h400">
						Camera detail
					</div>
					<div className="page-main-heading">Error - invalid camera</div>
				</div>
			);
		}
	}

	private renderQuestionData(sensor: any) {
		let data = sensor.data.length > 0
			? (sensor.sensorType === SensorType.COUNT ? sensor.data[0].label : (sensor.data[0].label ? "YES" : "NO"))
			: "NO DATA";
		return data;
	}

	private renderQuestionRow(uid: string, body: string, data: string) {
		return (
			<div key={uid} className="question-row row-entry">
				<div className="question-body">
					<span onClick={() => navigate(`/sensor/${uid}`)}>
						{body}
					</span>
				</div>

				<div>
					{data}
				</div>
			</div>
		);
	}

	private renderCredentials(auth: any, i: number) {
		if (!auth) { return null; }

		let credentialsSection;

		if (auth.protocol === "FTP" || auth.protocol === "HTTP_UPLOAD") {
			let username = auth.credentials.split(":")[0];
			let password = auth.credentials.split(":")[1];
			credentialsSection = (
				<div className="credentials">
					<div>
						Username: <span className="mono"><ClickToCopy content={username} /></span>
					</div>
					<div>
						Password: <span className="mono"><ClickToCopy content={password} /></span>
					</div>
				</div>
			);
		} else {
			credentialsSection = (
				<div className="credentials">
					Credentials: <span className="mono"><ClickToCopy content={auth.credentials} /></span>
				</div>
			);
		}

		return (
			<div className="credentials-block" key={i}>
				<div>
					Protocol: <span className="mono">{auth.protocol}</span>
				</div>
				{credentialsSection}
			</div>
		);
	}

	private getState(): State {
		return {
			device: DeviceDetailsStore.device,
		};
	}

	private onBackgroundLoad() {
		if (this.imgElement) {
			this.setState({
				size: [this.imgElement.width, this.imgElement.height]
			});
			this.forceUpdate();
		}
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

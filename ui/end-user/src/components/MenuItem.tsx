"use strict";

import "./PageElements.scss";

import { Icon } from "@blueprintjs/core";
import { IconName } from "@blueprintjs/icons";

import * as React from "react";

import UserStore from "../stores/UserStore";

import { navigate } from "../actions/navigation-actions";

type Props = {
	icon?: IconName;
	hoverType?: HoverTypes
	leftDivider?: boolean;
	rightDivider?: boolean;
	onClick?: () => void;
	linkText: string;
};

export enum HoverTypes {
	BORDER_BOTTOM = "hover-border-bottom",
	BACKGROUND_FILL = "hover-background-fill"
}

type State = {};

// Can do lookup type to make sure it's part of props
// SAFE THIS FOR LATER IF WE NEED MORE DYNAMIC MENUS
// type NavigationItem = {
// 	href: // function
// }

// React component allows for type control on props and state (state is empty here, props is as defined)
// Props ARE passed in, state is NOT passed in
export default class FutureNav extends React.Component<Props, {}> {
	public render() {
		let dividers = "";
		if (this.props.leftDivider) {
			dividers += " left-divider";
		}
		if (this.props.rightDivider) {
			dividers += " right-divider";
		}
		return (
			<div className={`zs-navigation-item${dividers}`} onClick={this.props.onClick}>
				{this.props.icon ?
					<span className={`pt-icon-standard pt-icon-${this.props.icon}`}/> : false}
				<span className={this.props.icon ? "zs-icon-navigation-text" : ""}>{this.props.linkText}</span>
			</div>
		);
	}

	private getState(): State {
		return {
			expanded: true
		};
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

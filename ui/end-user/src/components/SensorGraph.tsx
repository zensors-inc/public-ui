"use strict";

import * as React from "react";

import ReactEcharts from "echarts-for-react";

import * as echarts from "echarts/lib/echarts";

type Props = {
	data: {
		timestamp: Date;
		label: number;
		noValue: boolean;
	}[];
};

type State = {
};

export default class SensorGraph extends React.Component<Props, State> {
	private date?: string[];
	private data?: number[];

	constructor(props: Props, context: any) {
		super(props, context);
	}

	public render() {
		return (<ReactEcharts option={this.getOption()} />);
	}

	private getOption() {
		this.getData(10000);
		return {
			tooltip: {
				trigger: "axis",
				position: (pt: [number, number]) => ([pt[0], "10%"])
			},
			title: {},
			xAxis: {
				type: "category",
				boundaryGap: false,
				data: this.date,
			},
			yAxis: {
				type: "value",
				boundaryGap: [0, "100%"],
				name: "COUNT",
				nameLocation: "middle",
				nameGap: 25,
			},
			dataZoom: [{
				type: "inside",
				start: 0,
				end: 10
			}, {
				start: 0,
				end: 10,
				// tslint:disable-next-line
				handleIcon: "M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z",
				handleSize: "80%",
				handleStyle: {
					color: "#fff",
					shadowBlur: 3,
					shadowColor: "rgba(0, 0, 0, 0.6)",
					shadowOffsetX: 2,
					shadowOffsetY: 2
				}
			}],
			textStyle: {
				color: "black",
				fontFamily: "Rubik",
			},
			series: [
				{
					name: "label",
					type: "line",
					smooth: false,
					symbol: "none",
					sampling: "average",
					itemStyle: {
						normal: {
							color: "rgb(66, 179, 185)"
						}
					},
					data: this.data
				}
			]
		};
	}

	private getData(count: number) {
		let data = this.props.data.filter((dp) => (!dp.noValue))
			.sort((a, b) => (Number(a.timestamp) - Number(b.timestamp)));
		this.date = data.map((dp) => {
			let now = new Date(dp.timestamp);
			return now.getMonth() + "/" + now.getDay() + " " + now.getHours() + ":" + now.getMinutes();
		});
		this.data = data.map((dp) => dp.label);
	}
}


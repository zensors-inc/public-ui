"use strict";

import { Classes, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import {createFtpDevice,
		createHttpPollingDevice,
		createHttpUploadDevice } from "../actions/device-actions";
import { navigate } from "../actions/navigation-actions";

import { Button, Form, Submit, TextInput } from "@zensors/core-ui";

import "./CreateDevice.scss";

enum CameraType {
	FTP = "FTP",
	HTTP_UPLOAD = "HTTP_UPLOAD",
	HTTP_POLLING = "HTTP_POLLING",
	NEST = "NEST"
}

type State = {
	cameraType?: CameraType;
	name: string;
	imageUrl: string;
};

export default class CreateDevice extends React.Component<{}, State> {
	public constructor(props: {}, context: any) {
		super(props, context);
		this.state = {
			name: "",
			imageUrl: "",
		};
	}

	public render() {
		return (
			<div className="container">
				<div className="page-subheading h400">
					Manage Cameras
				</div>

				<div className="page-main-heading">
					Add Camera
				</div>

				<div className="camera-name-section">
					<div className="camera-section-label"> Camera name </div>
					<TextInput
						placeholder="Name for camera"
						data={this.state.name}
						onChange={(val) => this.handleNameChange(val)}/>
				</div>

				<div className="camera-type-section">
					<div className="camera-section-label"> Connection type </div>
					<div> Select the way you want to connect the camera to the system </div>

					<div className="camera-type-input">
						<div className="camera-type-select">
							<div className="camera-type-input-radio">
								<div className="radio">
									<label onClick={() => this.setState({cameraType: CameraType.NEST})}>
										<input className="radio-input" type="radio"
												checked={this.state.cameraType === CameraType.NEST}/>
										<div className="radio-check"></div>
									</label>
								</div>
							</div>

							<div className="camera-type-label"
								onClick={() => this.setState({cameraType: CameraType.NEST})}>
								Nest (Not functional, yet)
							</div>

							<div className="camera-type-description">
								Log into you Nest account to link it to Zensors.
							</div>
						</div>

						<div className="camera-type-select">
							<div className="camera-type-input-radio">
								<div className="radio">
									<label onClick={() => this.setState({cameraType: CameraType.HTTP_UPLOAD})}>
										<input className="radio-input" type="radio"
												checked={this.state.cameraType === CameraType.HTTP_UPLOAD}/>
										<div className="radio-check"></div>
									</label>
								</div>
							</div>

							<div className="camera-type-label"
								onClick={() => this.setState({cameraType: CameraType.HTTP_UPLOAD})}>
								HTTP Upload
							</div>

							<div className="camera-type-description">
								What is HTTP upload?
							</div>
						</div>

						<div className="camera-type-select">
							<div className="camera-type-input-radio">
								<div className="radio">
									<label onClick={() => this.setState({cameraType: CameraType.FTP})}>
										<input className="radio-input" type="radio"
												checked={this.state.cameraType === CameraType.FTP}/>
										<div className="radio-check"></div>
									</label>
								</div>
							</div>

							<div className="camera-type-label"
								onClick={() => this.setState({cameraType: CameraType.FTP})}>
								FTP
							</div>

							<div className="camera-type-description">
								With FTP credentials Zensors will provide you with
								a username and password to connect your camera
								to our network.
							</div>
						</div>

						<div className="camera-type-select">
							<div className="camera-type-input-radio">
								<div className="radio">
									<label onClick={() => this.setState({cameraType: CameraType.HTTP_POLLING})}>
										<input className="radio-input" type="radio"
												checked={this.state.cameraType === CameraType.HTTP_POLLING}/>
										<div className="radio-check"></div>
									</label>
								</div>
							</div>

							<div className="camera-type-label"
								onClick={() => this.setState({cameraType: CameraType.HTTP_POLLING})}>
								HTTP Polling
							</div>

							<div className="camera-type-description">
								What is HTTP polling?
								{this.state.cameraType === CameraType.HTTP_POLLING
									? <TextInput placeholder="Image URL"
										data={this.state.imageUrl}
										onChange={(val) => this.handleImageUrlChange(val)} />
									: null}
							</div>
						</div>
					</div>
				</div>

				<div className="confirm-button">
					<Button label="Confirm" className="center"
						disabled={!this.validateInput()}
						onClick={ () => this.handleConfirm() }/>
				</div>

			</div>
		);
	}

	private handleNameChange(val: string) {
		this.setState({name: val});
	}

	private handleImageUrlChange(val: string) {
		this.setState({imageUrl: val});
	}

	private validateInput() {
		if (!this.state.name || !this.state.cameraType) {
			return false;
		}

		if (this.state.cameraType === CameraType.HTTP_POLLING
			&& !this.state.imageUrl) {
			return false;
		}

		return true;
	}

	private handleConfirm() {
		switch (this.state.cameraType) {
			case CameraType.FTP:
				createFtpDevice({name: this.state.name}).then((uid) => navigate("/device/" + uid));
				break;

			case CameraType.HTTP_UPLOAD:
				createHttpUploadDevice({name: this.state.name}).then((uid) => navigate("/device/" + uid));
				break;

			case CameraType.HTTP_POLLING:
				createHttpPollingDevice({name: this.state.name, url: this.state.imageUrl})
					.then((uid) => navigate("/device/" + uid));
				break;

			case CameraType.NEST:
				console.log("How to create a NEST device?");
				break;

			default:
				console.log("should never happen");
		}
	}
}

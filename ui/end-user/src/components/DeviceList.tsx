"use strict";

import { Button, Classes, InputGroup } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import * as React from "react";

import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";

import { createSensorDeviceFilter } from "../actions/create-sensor-actions";
import { navigate } from "../actions/navigation-actions";

import { TextInput } from "@zensors/core-ui";

import "./DeviceList.scss";

type Props = {
	history: History;
};

type State = {
	devices?: DeviceListing[];
};

export default class DeviceList extends React.Component<Props, State> {
	public constructor(props: Props, context: any) {
		super(props, context);
		createSensorDeviceFilter("");
		this.state = this.getState();
	}

	public componentDidMount() {
		DeviceListStore.addDeviceListUpdateListener(() => this.updateState());
	}

	public componentWillUnmount() {
		DeviceListStore.removeDeviceListUpdateListener(() => this.updateState());
	}

	public render() {
		if (this.state.devices) {
			return (
				<div className="container">
					<div className="page-subheading h400">
						Manage Cameras
					</div>
					<div className="page-main-heading">Cameras</div>

					<div className="page-action-label"
						onClick={() => navigate("/device/new") }>
						Add camera
					</div>

					<div className="camera-search-input">
						<TextInput placeholder="Search cameras"
							onChange={(value) => {
								createSensorDeviceFilter(value);
						}}/>
					</div>

					<div className="device-list">
						{this.state.devices.map((device) =>
							<div key={device.uid} className="device-listing">
								<div className="camera-image-frame"
									key={device.uid}
									onClick={() => navigate(`/device/${device.uid}`)}
								>
									{ device.latest
										? <div>
											<img src={"//figzbuild.andrew.cmu.edu" + device.latest.url} />
											<div className="last-updated">Last updated
												{" " + new Date(device.latest.timestamp).toString()}</div>
											</div>
										: <div></div> }
								</div>
								<h4 className="device-name">{device.name}</h4>
							</div>)}
					</div>
				</div>
			);
		} else {
			return (
				<main id="device-list">
					<h2>Devices</h2>
					<div>Error loading devices.</div>
				</main>
			);
		}
	}

	private getState(): State {
		return {
			devices: DeviceListStore.filteredDevices
		};
	}

	private updateState(): void {
		this.setState(this.getState());
	}
}

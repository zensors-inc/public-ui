"use strict";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import SensorStore, { SensorDetails as Sensor } from "../stores/SensorStore";

import { navigate } from "../actions/navigation-actions";
import { loadSensorDetails } from "../actions/sensor-actions";

import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";

import PolygonOverlay from "./PolygonOverlay";
import SensorGraph from "./SensorGraph";
import ZensorGraph from "./ZensorGraph";

import "./QuestionDetails.scss";

type Props = {
	uid: string;
};

type State = {
	sensor?: Sensor;
};

export default class QuestionDetails extends React.Component<Props, State> {
	public constructor(props: Props, context: any) {
		super(props, context);
		SensorStore.addSensorDetailsUpdateListener(() => this.updateState());

		if (SensorStore.sensor && SensorStore.sensor.uid === this.props.uid) {
			this.state = this.getState();
		} else {
			this.state = {};
			loadSensorDetails(this.props.uid);
		}
	}

	public render() {

		if (this.state.sensor) {
			let device = DeviceListStore.devices.filter(({uid}) => uid === this.state.sensor!.device.uid)[0];
			let imageUrl = device && device.latest
				? ("//figzbuild.andrew.cmu.edu" + device.latest.url)
				: undefined;
			let cropArea = this.state.sensor.cropArea
				.match(/\(\d*(\.\d*)?,\d*(\.\d*)?\)/g)!
				.map((pt) => pt)
				.map((pt) => pt.substring(1, pt.length - 1))
				.map((pt) => pt.split(",").map((coord) => Number(coord)) as [number, number]);

			return (
				<div className="container">
					<div className="page-subheading h400">
						<span className="navigation-label"
							onClick={ () => navigate("/sensor/questions") }>
							Manage Questions
						</span>

						<span> > Question detail</span>
					</div>
					<div className="page-main-heading">{this.state.sensor.question}</div>

					<div className="question-details-grid">
						<div className="question-details-content">

							<div className="question-details-content-header h500">
								Details
							</div>

							<div className="question-details-edit-button">
								Edit details
							</div>

							<div className="question-details-content-row">
								<span className="question-details-content-label">
									Question Type:&nbsp;
								</span>
								<span className="h200">
									{this.renderQuestionType(this.state.sensor.sensorType)}
								</span>
							</div>

							<div className="question-details-content-row">
								<span className="question-details-content-label">
									Camera:&nbsp;
								</span>
								<span className="h200">
									{this.state.sensor.device.name}
								</span>
							</div>

							<div className="question-details-content-row">
								<span className="question-details-content-label">
									Started:&nbsp;
								</span>
								<span className="h200">
									2/26/2018 at 12:00am (Dummy)
								</span>
							</div>

							<div className="question-details-content-row">
								<span className="question-details-content-label">
									Sample rate:&nbsp;
								</span>
								<span className="h200">
									{this.renderSampleRate(this.state.sensor.frequency)}
								</span>
							</div>

						</div>

						<div className="question-details-image">
							<PolygonOverlay
								url={imageUrl}
								crop={cropArea}
							/>
						</div>

						<div className="question-details-viz">
							{ this.state.sensor.data.length > 0
								? <SensorGraph data={this.state.sensor.data} />
								: <div className="h500"> No data available </div>
							}
						</div>

					</div>

				</div>
			);
		} else {
			return (
				<div className="container">
					<div className="page-subheading h400">
						<span className="navigation-label"
							onClick={ () => navigate("/sensorui/questions") }>
							Manage Questions
						</span>

						<span> > Question detail</span>
					</div>
					<div className="page-main-heading">Error -- invalid sensor</div>

					<div className="question-details-grid"></div>
				</div>
			);
		}
	}

	private getState(): State {
		return {
			sensor: SensorStore.sensor
		};
	}

	private updateState(): void {
		this.setState(this.getState());
	}

	private renderQuestionType(sensorType: SensorType): string {
		switch (sensorType) {
			case SensorType.COUNT:
				return "Count";

			case SensorType.YES_NO:
				return "Yes or no";

			default:
				return "Error";
		}
	}

	private renderSampleRate(frequency: number): string {
		switch (frequency) {
			case 60:
				return "1 minute";

			case 300:
				return "5 minutes";

			case 3600:
				return "1 hour";

			case 86400:
				return "1 day";

			default:
				return frequency + " seconds";
		}
	}
}

"use strict";

import * as React from "react";

import { Classes, Tag } from "@blueprintjs/core";
import { DateInput } from "@blueprintjs/datetime";

import DayPickerInput from "react-day-picker/DayPickerInput";

import "react-day-picker/lib/style.css";
import "./DatePicker.scss";

import {
	Checkbox,
	CheckboxGroup,
	Input,
	InputProps,
	inputSymbol,
	TextInput
} from "@zensors/core-ui";

import CreateSensorStore, {CreateSensorDetails} from "../stores/CreateSensorStore";
import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";
import SensorListStore, { Filter, SensorListing } from "../stores/SensorListStore";

import { updateSensorFilter } from "../actions/sensor-actions";

import { SensorType } from "@zensors/public-gql-client";

// T isn't really used here? Not passed into generic type
type Props<T, V> = {
	label?: string;
	options?: { key: string, value: V }[];
};


// [Marvin] The only reason I'm doing all of this here is because otherwise we can't access this.state.dropdownOpen
// LMK if you know a cleaner fix for this
type S = {
	filteredSensors: SensorListing[],
	filter: Filter,
	tags: string[],
	date: Date,
	selected?: { [key: string]: boolean }
};

export default class SearchFilter<T, V> extends Input<T, V, V, Props<T, V>, S> {
	public static [inputSymbol] = true;
	// replace this to load tags from state or DB
	// Basically go over all sensors and extract their tags, then group or count
	private initialTags = ["London", "New York", "San Francisco", "Seattle"];
	public constructor(props: Props<T, V>, context: any) {
		super(props, context);
		this.state = this.getState();
		// init empty selected array
	}
	public getState() {
		return {
			devices: DeviceListStore.devices,
			filteredSensors: SensorListStore.filteredSensors,
			filter: SensorListStore.filter,
			// Make this dynamic / turn it into a grouping function
			tags: this.initialTags,
			date: new Date(),
			selected: this.initializeTags(this.initialTags)
		};
	}
	public updateState() {
		// It's only updating whatever it gets from getState and leaves the rest alone
		this.setState(this.getState());
	}

	public render() {
		// console.log(this.getState());
		// console.log(this.state);
		const {tags} = this.state;
		const tagElements = tags.map((tag) => (
			<Tag
				large={true}
				interactive={true}
				className={`zs-tag ${(this.state.selected && this.state.selected[tag]) ? "active" : "default"}`}
				key={tag}
				onClick={() => this.toggleTag(tag)}
			>
				{tag}
			</Tag>
		));

		// spread operator to assign variables with matching values from state
		return (
				<div className="filter-panel">
					<div className="filterbox">
					<h3 className="h300">Text search</h3>
					<TextInput
						placeholder="Search for sensors"
						// Right now it's not typechecking this onChange?
						onChange={(value) => {
							this.updateFilter({filterText: value});
						}}
					/>

					{/*
					<div className="filterbox">
						{tagElements}
					</div>
					*/}

					<CheckboxGroup
						header="Status"
						options={[
							{key: "Active", value: true},
							{key: "Inactive", value: false},
						]}
						onChange={ (val) => this.updateFilter({
							questionStatus: val,
						}) }
					/>

					<CheckboxGroup
						header="Question type"
						options={[
							{key: "Yes / No", value: SensorType.YES_NO},
							{key: "Count", value: SensorType.COUNT},
						]}
						onChange={ (val) => this.updateFilter({
							questionType: val,
						}) }
					/>

					<div className="filterbox">
					<h3 className="h300">Start date</h3>
					{<DayPickerInput format="YYYY-MM-DD" onDayChange={(day: any) => console.log(day)} />}
					</div>
					</div>
				</div>
		);
	}

	private toggleTag(filter: string) {
		// console.log("selected state", this.state.selected);
		let mutationState = (this.state.selected) ? this.state.selected : {};
		// If it exists and isn't undefined toggle it to the opposite of what it is
		if (mutationState && mutationState[filter] !== undefined) {
			mutationState[filter] = !mutationState[filter];
		}
		this.setState({selected: mutationState});
		this.updateFilter({toggles: mutationState});
	}

	private updateFilter(filterUpdateObject: Partial<Filter>) {
		// Assign the update object to the filter, should match Filter type
		let updatedFilter = SensorListStore.filter;
		updatedFilter = Object.assign(updatedFilter, filterUpdateObject);
		// Store it and filter results using the filter
		updateSensorFilter(updatedFilter);
	}

	private initializeTags(tags: string[]) {
		let tagObject = (this.state && this.state.selected) ? this.state.selected : {};
		tags.forEach((tag) => {
			tagObject[tag] = (tagObject[tag]) ? tagObject[tag] : false;
		});
		// console.log("Logging tagObject", tagObject);
		return tagObject;
	}
}

"use strict";

import * as React from "react";

import { DeviceDetails } from "../stores/DeviceStore";

import "./PolygonOverlay.scss";

type Props = {
	url?: string;
	crop?: [number, number][];
};

type State = {
	size?: [number, number];
};

export default class PolygonOverlay extends React.Component<Props, State> {
	private imgElement?: HTMLImageElement | null;
	private svgElement?: SVGSVGElement | null;

	public constructor(props: Props, context: any) {
		super(props, context);
		this.state = {};
	}

	public render() {
		let polygonStr = this.props.crop
			? this.props.crop.map(([x, y]) => `${x},${y}`).join(" ")
			: "";

		return (
			<div className="polygon-overlay-container">

				<img
					src={this.props.url}
					onLoad={() => this.onBackgroundLoad()}
					ref={(elt) => { this.imgElement = elt; }}
				/>

				{ polygonStr && this.state.size
					?
						<svg
							className="polygon-overlay"
							viewBox="0 0 1 1"
							preserveAspectRatio="none"
							width={this.state.size ? this.state.size[0] : 0}
							height={this.state.size ? this.state.size[1] : 0}
							ref={(elt) => this.svgElement = elt}
							style={{
								strokeWidth: 2 / this.state.size[1],
								strokeDasharray: `${8 / this.state.size[1]}`
							}}
						>
							<defs>
								<mask id="polygon">
									<rect className="mask-back" x={0} y={0} width={1} height={1} />
									<polygon className="mask-front" points={polygonStr} />
								</mask>
							</defs>
							<polygon points={polygonStr} />
							<rect className="darken" x={0} y={0} width={1} height={1} mask="url(#polygon)" />
						</svg>
					: undefined }

			</div>
		);
	}

	private onBackgroundLoad() {
		if (this.imgElement) {
			this.setState({
				size: [this.imgElement.width, this.imgElement.height]
			});
			this.forceUpdate();
		}
	}
}


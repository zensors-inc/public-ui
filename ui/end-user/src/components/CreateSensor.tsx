"use strict";

import { IconNames } from "@blueprintjs/icons";

import "./CreateSensor.scss";

import * as React from "react";

import { SensorType } from "@zensors/public-gql-client";

import {
	createSensorCropAreaUpdate,
	createSensorDeviceUpdate,
	createSensorFrequencyUpdate,
	createSensorNameUpdate,
	createSensorQuestionUpdate,
	createSensorReset,
	createSensorTypeUpdate,
	keepCreateSensorFormData,
} from "../actions/create-sensor-actions";
import { navigate } from "../actions/navigation-actions";
import { createSensor } from "../actions/sensor-actions";

import CreateSensorStore, {CreateSensorDetails} from "../stores/CreateSensorStore";
import DeviceListStore, { DeviceListing } from "../stores/DeviceListStore";

import DeviceDropdown from "./DeviceDropdown";
import PolygonInput from "./PolygonInput";

import {
	Button,
	ButtonStyle,
	Dropdown,
	RadioButton,
	TextInput
} from "@zensors/core-ui";

type FormType = {
	name: string,
	question: string,
	deviceUid: string,
	frequency: number,
	sensorType: SensorType,
	cropArea: [number, number][],
	valid: boolean,
};

type State = {
	devices: DeviceListing[];
	filteredDevices: DeviceListing[];
	form: CreateSensorDetails;
};


export default class CreateSensor extends React.Component<{}, State> {
	public constructor(props: {}, context: any) {
		super(props, context);
		this.state = this.getState();
	}

	// These listen to store changes and then update the state, so if the state is referenced
	// in the props, it will update the props and rerender the component to which the props are
	// passed in.
	public componentDidMount() {
		DeviceListStore.addDeviceListUpdateListener(() => this.updateState());
		CreateSensorStore.addCreateSensorUpdateListener(() => this.updateState());
		if ( this.state.form.keepFormData === false ) {
			createSensorReset();
		}

		if (this.state.form.keepFormData === true) {
			keepCreateSensorFormData(false);
		}
	}

	public componentWillUnmount() {
		DeviceListStore.removeDeviceListUpdateListener(() => this.updateState());
		CreateSensorStore.removeCreateSensorUpdateListener(() => this.updateState());
	}

	public getState() {
		return {
			devices: DeviceListStore.devices,
			form: CreateSensorStore.sensorForm,
			filteredDevices: DeviceListStore.filteredDevices
		};
	}

	public updateState() {
		this.setState(this.getState());
	}

	public render() {
		let deviceFilterDropdownOptions;
		if (this.state.filteredDevices.length > 0) {
			deviceFilterDropdownOptions = DeviceListStore.filteredDevices;
		} else {
			deviceFilterDropdownOptions = DeviceListStore.devices;
		}
		return (
			<div className="container">

				<div className="page-subheading h400">Manage Questions</div>
				<div className="page-main-heading"> New Question </div>
				<div className="content-grid">

				<div className="camera-selection">
					<div className="section-heading">
					Select a camera and region
					</div>

					<DeviceDropdown<FormType, string>
						options={deviceFilterDropdownOptions.map(({ uid, name }) => ({ key: name, value: uid }))}
						onChange={createSensorDeviceUpdate}
					/>
				</div>

				<div className="question-type">
					<div className="section-heading">
					What type of question would you like to ask?
					</div>

					<RadioButton<FormType, SensorType>
						options={[
								{ key: "Count", value: SensorType.COUNT },
								{ key: "Yes-No", value: SensorType.YES_NO }
							]}
						onChange={createSensorTypeUpdate}
						data={this.state.form.sensorType}
					/>
				</div>


				<div className="question-input">
					<div className="section-heading">
						Write a question
					</div>

					<div className="section-description">
						Please enter a question.
					</div>

					<TextInput<FormType>
						label="name"
						placeholder="Sensor Name"
						onChange={createSensorNameUpdate}
						data={this.state.form.name}
					/>

					<TextInput<FormType>
						label="question"
						placeholder="Question"
						onChange={createSensorQuestionUpdate}
						data={this.state.form.question}
					/>
				</div>

				<div className="sample-rate">
					<div className="section-heading">
						Pick a sample rate
					</div>

					<div className="section-description">
						One frame will be sent per time period.
						Select an option to learn more about each.
					</div>


					<RadioButton<FormType, number>
						options={[
								{ key: "1 MINUTE", value: 60 },
								{ key: "5 MINUTES", value: 300 },
								{ key: "1 HOUR", value: 3600 },
								{ key: "1 DAY", value: 86400}
							]}
						annotations = {[
								{
									key: "1 MINUTE",
									annotation: "Best for frequent events or events with "
									+ "short durations like counting visitors in a reception area."
								},
								{
									key: "5 MINUTES",
									annotation: "Great for somewhat frequent events that happen "
									+ "more regularly like monitoring the sink for dishes."
								},
								{
									key: "1 HOUR",
									annotation: "For less frequent events like tracking shared work"
									+ " space utilization over time."
								},
								{
									key: "1 DAY",
									annotation: "Best for events that happen around the same time "
									+ "everyday and need to be tracked over time."
								}
							]}
						onChange={createSensorFrequencyUpdate}
						data={this.state.form.frequency}
					/>
				</div>


				<div className="image-frame">
					<PolygonInput<FormType>
						onChange={createSensorCropAreaUpdate}
						data={{ area: this.state.form.cropArea, deviceUid: this.state.form.deviceUid }}
					/>
				</div>
				</div>

				<div className="review-and-confirm-button">
					<Button label="Review and Confirm"
						disabled={!this.state.form.valid}
						onClick={() => this.submitCreateSensor()}
						className="center"/>
				</div>
			</div>
		);
	}

	private submitCreateSensor() {
		if (!this.state.form.valid) {
			return;
		}

		// Only navigate, actually create the sensor on the confirmation page
		navigate("/sensor/confirmation");
	}
}

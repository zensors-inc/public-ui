"use strict";

import * as EventEmitter from "event-emitter";

import { SensorType } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

import { refreshDeviceList } from "../actions/device-actions";

export interface SensorListing {
	uid: string;
	name: string;
	question: string;
	frequency: number;
	active: boolean;
	sensorType: SensorType;
	device: {
		name: string,
		uid: string
	};
}

// Work in progress
export interface Filter {
	filterText: string;
	dateRange: [number, number] | null;
	// TODO: connect tag toggles to updating this part of the filter
	toggles: {};
	// These string definitions are placeholders. Probably do enums?
	// accepts sensors matching at one of the following criteria
	questionStatus: boolean[];
	questionType: SensorType[];
}

class SensorListStore {
	public sensors: SensorListing[];
	public filteredSensors: SensorListing[];
	// Store global filter so we can change filter options independently
	public filter: Filter;
	private emitter: EventEmitter.Emitter;

	public constructor() {
		this.sensors = [];
		this.filteredSensors = [];
		this.filter = {
			filterText: "",
			dateRange: null,
			toggles: {},
			questionStatus: [],
			questionType: [],
		};

		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {
				case "sensor-list-load":
					this.sensors = action.data;
					this.updateFilteredSensorList();
					this.emitter.emit("sensor-list-update");
					break;

				case "sensor-creation-complete":
					refreshDeviceList();
					break;
				// This actually filters
				case "create-sensor-filter":
					this.filteredSensors = action.data.results;
					this.updateFilteredSensorList();
					this.emitter.emit("sensor-list-update");
					break;

				case "update-sensor-filter":
					this.filter = action.data;
					this.updateFilteredSensorList();
					this.emitter.emit("sensor-list-update");
					break;

				case "reset-sensor-filter":
					this.resetSensorFilter();
					this.emitter.emit("sensor-list-update");
					break;
			}
		});
	}

	public addSensorListUpdateListener(fn: () => any) {
		this.emitter.on("sensor-list-update", fn);
	}

	public removeSensorListUpdateListener(fn: () => any) {
		this.emitter.off("sensor-list-update", fn);
	}

	private resetSensorFilter() {
		this.filteredSensors = this.sensors;
		this.filter = {
			filterText: "",
			dateRange: null,
			toggles: {},
			questionStatus: [],
			questionType: [],
		};
	}

	private updateFilteredSensorList() {
		let re = new RegExp(this.filter.filterText, "gi");
		let newFilteredList = [] as SensorListing[];
		for (let question of this.sensors) {
			if (question.question.match(re) === null) {
				continue;
			}

			if (this.filter.questionStatus.length !== 0
				&& this.filter.questionStatus.indexOf(question.active) === -1) {
				continue;
			}

			if (this.filter.questionType.length !== 0
				&& this.filter.questionType.indexOf(question.sensorType) === -1) {
				continue;
			}

			// only if this question passes all filter criteria
			// (not triggering any break statement)
			newFilteredList.push(question);
		}
		this.filteredSensors = newFilteredList;
	}
}

export default new SensorListStore();

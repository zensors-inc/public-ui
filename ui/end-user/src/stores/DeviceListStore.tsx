"use strict";

import * as EventEmitter from "event-emitter";

import Dispatcher from "../dispatcher/Dispatcher";

import { refreshDeviceList } from "../actions/device-actions";

export interface DeviceListingFilter {
	filterText: string;
}

export interface DeviceListing {
	uid: string;
	name: string;
	latest?: { timestamp: Date, url: string };
}

class DeviceListStore {
	// Declarations
	public devices: DeviceListing[];
	public filteredDevices: DeviceListing[];
	private emitter: EventEmitter.Emitter;

	public constructor() {
		// Instantiations
		this.devices = [];
		this.filteredDevices = [];
		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {
				case "device-list-load":
					this.devices = action.data;
					this.emitter.emit("device-list-update");
					break;

				case "device-creation-complete":
					refreshDeviceList();
					break;

				case "create-sensor-device-filter":
					this.filteredDevices = action.data.results;
					this.emitter.emit("device-list-update");
					break;
			}
		});
	}

	public addDeviceListUpdateListener(fn: () => any) {
		this.emitter.on("device-list-update", fn);
	}

	public removeDeviceListUpdateListener(fn: () => any) {
		this.emitter.off("device-list-update", fn);
	}
}

export default new DeviceListStore();

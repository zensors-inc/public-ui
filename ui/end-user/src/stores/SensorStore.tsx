"use strict";

import * as EventEmitter from "event-emitter";

import { SensorType } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

export interface SensorDetails {
	uid: string;
	name: string;
	question: string;
	frequency: number;
	cropArea: string;
	sensorType: SensorType;
	device: {
		name: string;
		uid: string;
	};
	data: {
		timestamp: Date;
		label: number;
		noValue: boolean;
	}[];
}

class DeviceListStore {
	public sensor?: SensorDetails;

	private emitter: EventEmitter.Emitter;

	public constructor() {
		this.sensor = undefined;

		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {
				case "sensor-details-load":
					this.sensor = action.data;
					this.emitter.emit("sensor-details-update");
					break;
			}
		});
	}

	public addSensorDetailsUpdateListener(fn: () => any) {
		this.emitter.on("sensor-details-update", fn);
	}

	public removeSensorDetailsUpdateListener(fn: () => any) {
		this.emitter.off("sensor-details-update", fn);
	}
}

export default new DeviceListStore();

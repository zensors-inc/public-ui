"use strict";

import * as EventEmitter from "event-emitter";

import Dispatcher from "../dispatcher/Dispatcher";
import { SensorDetails } from "../stores/SensorStore";

import { loadSensorDetails } from "../actions/dashboard-actions";

import { SensorType } from "@zensors/public-gql-client";

export interface SensorListing {
	uid: string;
	name: string;
	question: string;
	frequency: number;
	active: boolean;
	sensorType: SensorType;
	device: {
		name: string,
		uid: string
	};
}


class DashboardStore {
	public visualizationSensors: SensorDetails[];
	// public filteredSensors: SensorListing[];
	public uids: string[];
	// Store global filter so we can change filter options independently
	private emitter: EventEmitter.Emitter;

	public constructor() {
		this.uids = [];
		// Needs an action to initialize this from server based on user config...
		// Right now it's empty on every restart
		this.visualizationSensors = [];

		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {

				// Add or remove from the flux array containing IDs that should be visualized
				case "update-visualization-list":
					this.uids = action.data.results;
					if (action.data.type === "added") {
						// Load only data for the new UID
						console.log(`Getting sensor data for ${action.data.uid}`);
						loadSensorDetails(action.data.uid);
					} else if (action.data.type === "removed") {
						this.visualizationSensors = this.visualizationSensors.filter((sensor) => sensor.uid !== action.data.uid);
						this.emitter.emit("visualization-list-update");
					}

					break;

				// loadSensorDetails fires off this action if it fetches details
				case "visualization-sensor-details-load":
					console.log(action.data);
					console.log("Fired final emitter event");
					this.updateVisualizationList(action.data);
					console.log(action.data);
					this.emitter.emit("visualization-list-update");
			}
		});
	}

	public addSensorListUpdateListener(fn: () => any) {
		this.emitter.on("visualization-list-update", fn);
	}

	public removeSensorListUpdateListener(fn: () => any) {
		this.emitter.off("visualization-list-update", fn);
	}

	private updateVisualizationList(elem: SensorDetails) {
		let newList = [] as SensorListing[];
		// Update the visualized sensors with the sensor that was just added or removed
		// VisualizationList sensors for now just contains the entire SensorDetails object
		// Can slim this down a bit later
		this.visualizationSensors.push(elem);
	}
}

export default new DashboardStore();

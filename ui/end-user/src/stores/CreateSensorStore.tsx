"use strict";

import * as EventEmitter from "event-emitter";

import { SensorType } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

export interface CreateSensorDetails {
	cropArea: [number, number][];
	deviceUid: string | undefined;
	frequency: number | undefined;
	name: string;
	question: string;
	sensorType: SensorType | undefined;
	valid: boolean;						// default to false
	keepFormData: boolean;				// default to false
}

class CreateSensorStore {
	public sensorForm: CreateSensorDetails;

	private emitter: EventEmitter.Emitter;

	public constructor() {
		this.sensorForm = {
			cropArea: [],
			deviceUid: undefined,
			frequency: undefined,
			name: "",
			question: "",
			sensorType: undefined,
			valid: false,
			keepFormData: false,
		};

		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {
				case "create-sensor-device-update":
					this.sensorForm.deviceUid = action.data.uid;
					break;

				case "create-sensor-name-update":
					this.sensorForm.name = action.data.name;
					break;

				case "create-sensor-question-update":
					this.sensorForm.question = action.data.question;
					break;

				case "create-sensor-frequency-update":
					this.sensorForm.frequency = action.data.frequency;
					break;

				case "create-sensor-type-update":
					this.sensorForm.sensorType = action.data.type;
					break;

				case "create-sensor-crop-area-update":
					this.sensorForm.cropArea = action.data.cropArea;
					break;

				case "keep-create-sensor-form-data":
					this.sensorForm.keepFormData = action.data.keep;
					break;

				case "create-sensor-reset":
					this.resetSensorForm();
					break;
			}
			this.sensorForm.valid = this.validate();
			this.emitter.emit("create-sensor-update");
		});
	}

	public addCreateSensorUpdateListener(fn: () => any) {
		this.emitter.on("create-sensor-update", fn);
	}

	public removeCreateSensorUpdateListener(fn: () => any) {
		this.emitter.off("create-sensor-update", fn);
	}

	public validate() {
		if (this.sensorForm.cropArea.length < 3) {
			return false;
		}
		if (!this.sensorForm.deviceUid) {
			return false;
		}
		if (!this.sensorForm.frequency) {
			return false;
		}
		if (!this.sensorForm.name) {
			return false;
		}
		if (!this.sensorForm.question) {
			return false;
		}
		if (this.sensorForm.sensorType === undefined) {
			return false;
		}
		return true;
	}

	private resetSensorForm() {
		this.sensorForm = {
			cropArea: [],
			deviceUid: undefined,
			frequency: undefined,
			name: "",
			question: "",
			sensorType: undefined,
			valid: false,
			keepFormData: false,
		};
	}
}

export default new CreateSensorStore();

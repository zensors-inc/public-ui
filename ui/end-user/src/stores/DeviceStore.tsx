"use strict";

import * as EventEmitter from "event-emitter";

import { SensorType } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

import { refreshDeviceList } from "../actions/device-actions";

export interface DeviceDetails {
	uid: string;
	name: string;
	auth: { credentials: string, protocol: string }[];
	data: { timestamp: Date, url: string }[];
	sensors: {
		uid: string;
		name: string;
		cropArea: string;
		sensorType: SensorType;
		data: {
			timestamp: Date;
			label: number;
		}[];
	}[];
}

class DeviceListStore {
	public device?: DeviceDetails;
	public crop?: [number, number][];

	private emitter: EventEmitter.Emitter;

	public constructor() {
		this.device = undefined;

		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {
				case "device-details-load":
					this.device = action.data;
					this.emitter.emit("device-details-update");
					break;

				case "device-crop-display":
					this.crop = action.data;
					this.emitter.emit("device-details-update");
					break;

				case "device-creation-complete":
					refreshDeviceList();
					break;
			}
		});
	}

	public addDeviceDetailsUpdateListener(fn: () => any) {
		this.emitter.on("device-details-update", fn);
	}

	public removeDeviceDetailsUpdateListener(fn: () => any) {
		this.emitter.off("device-details-update", fn);
	}
}

export default new DeviceListStore();

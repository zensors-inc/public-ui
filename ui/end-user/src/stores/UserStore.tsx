"use strict";

import * as EventEmitter from "event-emitter";

import Dispatcher from "../dispatcher/Dispatcher";

class UserStore {
	public email?: string;
	public firstName?: string;
	public lastName?: string;
	public uid?: string;

	private emitter: EventEmitter.Emitter;

	public constructor() {
		this.email = undefined;
		this.firstName = undefined;
		this.lastName = undefined;
		this.uid = undefined;

		this.emitter = EventEmitter({});

		Dispatcher.register((action) => {
			switch (action.kind) {
				case "user-data-load":
					console.log(action.data);
					if (action.data) {
						this.email = action.data.email;
						this.firstName = action.data.firstName;
						this.lastName = action.data.lastName;
						this.uid = action.data.uid;
					} else {
						this.email = undefined;
						this.firstName = undefined;
						this.lastName = undefined;
						this.uid = undefined;
					}
					this.emitter.emit("user-data-load");
					break;
			}
		});
	}

	public addUserDataLoadListener(fn: () => any) {
		this.emitter.on("user-data-load", fn);
	}

	public removeUserDataLoadListener(fn: () => any) {
		this.emitter.off("user-data-load", fn);
	}
}

export default new UserStore();

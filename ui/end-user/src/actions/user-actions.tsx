"use strict";

import { Provider } from "nconf";

import { query } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

const nconf = new Provider();

nconf
	.env()
	.defaults({
		API_HOSTNAME: undefined,
		API_PORT: undefined,
		API_PROTOCOL: undefined
	});

const API_URL = `${nconf.get("API_PROTOCOL")}://${nconf.get("API_HOSTNAME")}:${nconf.get("API_PORT")}`;

async function submitForm(endpoint: string, body: object): Promise<string> {
	let response;

	try {
		response = await fetch(`${API_URL}${endpoint}`, {
			method: "POST",
			body: JSON.stringify(body),
			headers: {
				"content-type": "application/json"
			},
			credentials: "include"
		});
	} catch (e) {
		throw new Error("Network error");
	}

	if (response.ok) {
		return await response.text();
	} else {
		throw new Error(await response.text());
	}
}

export async function login(body: { email: string, password: string }): Promise<void> {
	try {
		await submitForm("/user/login", body);
		await loadUserData();
	} catch (e) {
		throw e;
	}
}

export async function register(body: {
			firstName: string,
			lastName: string,
			email: string,
			password: string,
			confirmPassword: string
		}): Promise<void> {
	if (body.password !== body.confirmPassword) {
		throw new Error("Passwords don't match.");
	}

	try {
		await submitForm("/user/register", body);
		await loadUserData();
	} catch (e) {
		throw e;
	}
}

export async function loadUserData() {
	try {
		let userData = await query(`
			query($startTime: Date, $endTime: Date) {
				self {
					email
					firstName
					lastName
					uid
					domain {
						devices {
							name
							uid
							data(time: { start: $startTime, end: $endTime }, pagination: { size: 1, index: 1 }) {
								timestamp
								url
							}
							sensors {
								uid
								name
								active
								question
								frequency
								sensorType
								device {
									name
									uid
								}
							}
						}
					}
				}
			}
		`, {
			startTime: new Date(0),
			endTime: new Date()
		});

		if (userData && userData.self) {
			Dispatcher.dispatch({
				kind: "user-data-load",
				data: {
					email: userData.self.email,
					firstName: userData.self.firstName,
					lastName: userData.self.lastName,
					uid: userData.self.uid,
				}
			});

			Dispatcher.dispatch({
				kind: "device-list-load",
				data: userData.self.domain.devices.map((device: any) => ({
					uid: device.uid,
					name: device.name,
					latest: device.data.length > 0 ? device.data[0] : undefined
				}))
			});

			let sensors = [] as any[];
			for (let device of userData.self.domain.devices) {
				sensors = sensors.concat(device.sensors);
			}

			Dispatcher.dispatch({
				kind: "sensor-list-load",
				data: sensors,
			});

		} else {
			Dispatcher.dispatch({
				kind: "user-data-load",
				data: undefined
			});
		}
	} catch (e) {
		Dispatcher.dispatch({
			kind: "user-data-load",
			data: undefined
		});
	}
}

"use strict";


import { query, SensorType } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

import DeviceListStore from "../stores/DeviceListStore";

export function createSensorDeviceUpdate(uid: string) {
	Dispatcher.dispatch({
		kind: "create-sensor-device-update",
		data: {uid}
	});
}

// Filter the devices in DeviceListStore.devices based on a string param
export function createSensorDeviceFilter(filter: string) {
	let unfiltered = DeviceListStore.devices;
	let re = new RegExp(filter, "gi");
	let filtered = unfiltered.filter((device) => device.name.match(re));
	Dispatcher.dispatch({
		kind: "create-sensor-device-filter",
		data: {results: filtered}
	});
}

export function createSensorNameUpdate(name: string) {
	Dispatcher.dispatch({
		kind: "create-sensor-name-update",
		data: {name}
	});
}

export function createSensorQuestionUpdate(question: string) {
	Dispatcher.dispatch({
		kind: "create-sensor-question-update",
		data: {question}
	});
}

export function createSensorFrequencyUpdate(frequency: number) {
	Dispatcher.dispatch({
		kind: "create-sensor-frequency-update",
		data: {frequency}
	});
}

export function createSensorTypeUpdate(type: SensorType) {
	Dispatcher.dispatch({
		kind: "create-sensor-type-update",
		data: {type}
	});
}

export function createSensorCropAreaUpdate(cropArea: [number, number][]) {
	Dispatcher.dispatch({
		kind: "create-sensor-crop-area-update",
		data: {cropArea}
	});
}


export function createSensorReset() {
	Dispatcher.dispatch({
		kind: "create-sensor-reset"
	});
}

export function keepCreateSensorFormData(keep: boolean) {
	Dispatcher.dispatch({
		kind: "keep-create-sensor-form-data",
		data: {keep},
	});
}

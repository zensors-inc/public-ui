"use strict";

import { query } from "@zensors/public-gql-client";

import Dispatcher from "../dispatcher/Dispatcher";

export async function createFtpDevice(data: { name: string }) {
	let result = await query(`
		mutation($name: String) {
			createFtpDevice(name: $name) {
				device {
					uid
				}
			}
		}
	`, {
		name: data.name
	});
	// TODO: we can use the result value here at some point... but it doesn't actually return the new device yet

	Dispatcher.dispatch({
		kind: "device-creation-complete",
		data: result.createFtpDevice.device.uid,
	});

	return result.createFtpDevice.device.uid;
}

export async function createHttpUploadDevice(data: { name: string }) {
	let result = await query(`
		mutation($name: String) {
			createHttpUploadDevice(name: $name) {
				device {
					uid
				}
			}
		}
	`, {
		name: data.name
	});
	// TODO: we can use the result value here at some point... but it doesn't actually return the new device yet

	Dispatcher.dispatch({
		kind: "device-creation-complete",
		data: result.createHttpUploadDevice.device.uid,
	});

	return result.createHttpUploadDevice.device.uid;
}

export async function createHttpPollingDevice(data: { name: string, url: string }) {
	let result = await query(`
		mutation($name: String) {
			createHttpPollingDevice(name: $name, url: $url) {
				device {
					uid
				}
			}
		}
	`, {
		name: data.name,
		url: data.url
	});
	// TODO: we can use the result value here at some point... but it doesn't actually return the new device yet

	Dispatcher.dispatch({
		kind: "device-creation-complete",
		data: result.createHttpPollingDevice.device.uid,
	});

	return result.createHttpPollingDevice.device.uid;
}

export async function refreshDeviceList() {
	let result = await query(`
		query($startTime: Date, $endTime: Date) {
			self {
				domain {
					devices {
						uid
						name
						data(time: { start: $startTime, end: $endTime }, pagination: { size: 1, index: 1 }) {
							timestamp
							url
						}
					}
				}
			}
		}
	`, {
		startTime: new Date(0),
		endTime: new Date()
	});

	if (result.self) {
		Dispatcher.dispatch({
			kind: "device-list-load",
			data: result.self.domain.devices
		});
	} else {
		// TODO(mdsavage): What do we do if the data load fails?
	}
}

export async function loadDeviceDetails(uid: string) {
	let result = await query(`
		query($uid: String, $startTime: Date, $endTime: Date) {
			deviceByUid(uid: $uid) {
				uid
				name
				auth {
					credentials
					protocol
				}
				data(time: { start: $startTime, end: $endTime }, pagination: { size: 1, index: 1 }) {
					timestamp
					url
				}
				sensors {
					uid
					name
					cropArea
					sensorType
					data(time: { start: $startTime, end: $endTime }, pagination: { size: 1, index: 1 }) {
						timestamp
						label
					}
				}
			}
		}
	`, {
		uid,
		startTime: new Date(0),
		endTime: new Date()
	});

	if (result.deviceByUid) {
		Dispatcher.dispatch({
			kind: "device-details-load",
			data: result.deviceByUid
		});
	}
}

export function setCropDisplay(data: [number, number][] | undefined) {
	Dispatcher.dispatch({
		kind: "device-crop-display",
		data
	});
}

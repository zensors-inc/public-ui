"use strict";

import { query, SensorType } from "@zensors/public-gql-client";

import SensorListStore, { Filter } from "../stores/SensorListStore";

import Dispatcher from "../dispatcher/Dispatcher";

interface SensorCreationData {
	name: string;
	deviceUid: string;
	question: string;
	frequency: number;
	sensorType: SensorType;
	cropArea: [number, number][];
}

export async function createSensor(data: SensorCreationData) {
	let sensorTypeString =
		data.sensorType === SensorType.COUNT ? "COUNT" :
		data.sensorType === SensorType.YES_NO ? "YES_NO" :
		undefined;

	let result = await query(`
		mutation($name: String, $deviceUid: String, $question: String,
				$frequency: Int, $sensorType: SensorType, $cropArea: String) {
			createSensor(name: $name, deviceUid: $deviceUid, question: $question, frequency: $frequency,
					sensorType: $sensorType, cropArea: $cropArea) {
				uid
			}
		}
	`, {
		name: data.name,
		deviceUid: data.deviceUid,
		question: data.question,
		frequency: data.frequency,
		sensorType: sensorTypeString,
		cropArea: `(${data.cropArea.map(([x, y]) => `(${x},${y})`).join(",")})` as string
	});

	Dispatcher.dispatch({ kind: "sensor-creation-complete" });
}

export async function refreshSensorList() {
	let result = await query(`
			query {
				self {
					sensors {
						uid
						name
						active
						question
						frequency
						sensorType
						device {
							name
							uid
						}
					}
				}
			}
	`, {});

	Dispatcher.dispatch({ kind: "sensor-list-load", data: result.self.sensors });
}

export async function loadSensorDetails(uid: string) {
	let result = await query(`
		query($uid: String, $startTime: Date, $endTime: Date) {
			sensorByUid(uid: $uid) {
				uid
				name
				question
				frequency
				cropArea
				sensorType
				device {
					name
					uid
				}
				data(time: { start: $startTime, end: $endTime }, pagination: { size: 100000, index: 0 }) {
					timestamp
					label
					noValue
				}
			}
		}
	`, {
		uid,
		startTime: new Date(0),
		endTime: new Date()
	});

	if (result.sensorByUid) {
		Dispatcher.dispatch({
			kind: "sensor-details-load",
			data: result.sensorByUid
		});
	}
}


export function createSensorFilter(filter: Filter) {
	// Figure out how to deal with the filter object here
	let unfiltered = SensorListStore.sensors;
	// Should extract the fields from filter and search in sensor fields
	let re = new RegExp(filter.filterText, "gi");
	// LOL: Figure out a better solution for this search than just adding stuff together
	let filtered = unfiltered.filter((sensor) => `${sensor.name} ${sensor.question}`.match(re));
	console.log(filtered);
	Dispatcher.dispatch({
		kind: "create-sensor-filter",
		data: {results: filtered}
	});
}

// REVIEW
// I don't think this is really needed, can just do everything in the createSensorFilter
export function updateSensorFilter(filter: Filter) {
	Dispatcher.dispatch({
		kind: "update-sensor-filter",
		data: filter
	});
}

export function resetSensorFilter() {
	Dispatcher.dispatch({
		kind: "reset-sensor-filter"
	});
}

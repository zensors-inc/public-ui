"use strict";

import { createBrowserHistory } from "history";
export const history = createBrowserHistory();

export function navigate(path: string): void {
	history.push(path);
}

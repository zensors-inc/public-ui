"use strict";



import { query, SensorType } from "@zensors/public-gql-client";

import DashboardStore from "../stores/DashboardStore";

import Dispatcher from "../dispatcher/Dispatcher";

import SensorListStore, { Filter } from "../stores/SensorListStore";



export async function loadSensorDetails(uid: string) {
	console.log("Firing");
	let result = await query(`
		query($uid: String, $startTime: Date, $endTime: Date) {
			sensorByUid(uid: $uid) {
				uid
				name
				question
				frequency
				cropArea
				sensorType
				device {
					name
					uid
				}
				data(time: { start: $startTime, end: $endTime }, pagination: { size: 1, index: 1 }) {
					timestamp
					label
					noValue
				}
			}
		}
	`, {
		uid,
		startTime: new Date(0),
		endTime: new Date()
	});

	console.log("Result:", result);
	if (result.sensorByUid) {
		Dispatcher.dispatch({
			kind: "visualization-sensor-details-load",
			data: result.sensorByUid
		});
	}
}


// Add or remove sensor visualization based on UID
export function toggleVisualization(uid: string) {
	// Figure out how to deal with the filter object here
	let current = DashboardStore.uids;
	console.log("Adding or removing this one:", uid);
	let updatedUID = uid;
	// Should extract the fields from filter and search in sensor fields
	let actionType: string;
	console.log(uid);
	console.log(current);
	if (current.indexOf(uid) > -1) {
		current = current.filter((e) => e !== uid);
		console.log("removing");
		actionType = "removed";
	} else {
		current.push(uid);
		actionType = "added";
	}

	console.log("Dispatching update visualization list action");
	Dispatcher.dispatch({
		kind: "update-visualization-list",
		data: {
			// This is a really ugly hack... We want to get the ID of the sensor that was
			// added or deleted, so we only need to fetch that one and not refresh the entire list
			results: current,
			type: actionType,
			uid: updatedUID
		}
	});
}


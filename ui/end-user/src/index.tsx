import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./components/App";

import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

import "@zensors/core-ui/styles/main.scss";

import registerServiceWorker from "./registerServiceWorker";

import { Button } from "@zensors/core-ui";

ReactDOM.render(
	<App />,
	document.getElementById("root") as HTMLElement
);

registerServiceWorker();

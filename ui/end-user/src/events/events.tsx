"use strict";

import { DeviceDetails } from "../stores/DeviceStore";
import { SensorDetails } from "../stores/SensorStore";

import { SensorType } from "@zensors/public-gql-client";
import { DeviceListing } from "../stores/DeviceListStore";
import { Filter, SensorListing } from "../stores/SensorListStore";

export type AppEvent =
	| UserDataLoadEvent
	| DeviceListLoadEvent
	| DeviceDetailsLoadEvent
	| DeviceCreationCompleteEvent
	| SensorListLoadEvent
	| SensorDetailsLoadEvent
	| SensorCreationCompleteEvent
	| DeviceCropDisplayEvent
	| CreateSensorDeviceUpdateEvent
	| CreateSensorNameUpdateEvent
	| CreateSensorQuestionUpdateEvent
	| CreateSensorFrequencyUpdateEvent
	| CreateSensorTypeUpdateEvent
	| CreateSensorCropAreaUpdateEvent
	| CreateSensorResetEvent
	| CreateSensorDeviceFilterEvent
	| KeepCreateSensorFormDataEvent
	| CreateSensorFilterEvent
	| UpdateSensorFilterEvent
	| ResetSensorFilterEvent
	| VisualizationSensorDetailsLoad
	| UpdateVisualizationList
	;

interface UpdateVisualizationList {
	kind: "update-visualization-list";
	// VisualizationList is an array of UIDs
	data: {
		results: string[];
		type: string;
		uid: string;
	};
}

interface VisualizationSensorDetailsLoad {
	kind: "visualization-sensor-details-load";
	// TODO: Fix this
	data: SensorDetails;
}

interface CreateSensorDeviceUpdateEvent {
	kind: "create-sensor-device-update";
	data: {
		uid: string;
	};
}

interface CreateSensorFilterEvent {
	kind: "create-sensor-filter";
	data: {
		results: SensorListing[];
	};
}

interface UpdateSensorFilterEvent {
	kind: "update-sensor-filter";
	data: Filter;
}

interface ResetSensorFilterEvent {
	kind: "reset-sensor-filter";
}

// Interfaces are type definitions, so here, we basically say that the interface CreateSensorDeviceFilterEvent
// has to have a kind with "creaate...filter", and that specific results config
// UNION the entire thing ^^^ and allow those types to be passed into the dispatcher.
// The action then creates a dispatch object to match anything in the union
// Interfaces are effectively used as filters that determine what is allowed to pass through to the dispatcher
interface CreateSensorDeviceFilterEvent {
	kind: "create-sensor-device-filter";
	data: {
		results: DeviceListing[];
	};
}


interface CreateSensorNameUpdateEvent {
	kind: "create-sensor-name-update";
	data: {
		name: string;
	};
}

interface CreateSensorQuestionUpdateEvent {
	kind: "create-sensor-question-update";
	data: {
		question: string;
	};
}

interface CreateSensorFrequencyUpdateEvent {
	kind: "create-sensor-frequency-update";
	data: {
		frequency: number;
	};
}

interface CreateSensorTypeUpdateEvent {
	kind: "create-sensor-type-update";
	data: {
		type: SensorType;
	};
}

interface CreateSensorCropAreaUpdateEvent {
	kind: "create-sensor-crop-area-update";
	data: {
		cropArea: [number, number][];
	};
}

interface CreateSensorResetEvent {
	kind: "create-sensor-reset";
}

interface KeepCreateSensorFormDataEvent {
	kind: "keep-create-sensor-form-data";
	data: {
		keep: boolean;
	};
}

interface UserDataLoadEvent {
	kind: "user-data-load";
	data?: {
		email: string;
		firstName: string;
		lastName: string;
		uid: string;
	};
}

interface DeviceListLoadEvent {
	kind: "device-list-load";
	data: {
		name: string;
		uid: string;
		latest?: { timestamp: Date, url: string };
	}[];
}

interface DeviceDetailsLoadEvent {
	kind: "device-details-load";
	data: DeviceDetails;
}

interface SensorListLoadEvent {
	kind: "sensor-list-load";
	data: { uid: string,
			name: string,
			question: string,
			frequency: number,
			active: boolean,
			sensorType: SensorType,
			device: {
				name: string,
				uid: string
			}
		}[];
}

interface SensorDetailsLoadEvent {
	kind: "sensor-details-load";
	data: SensorDetails;
}

interface DeviceCreationCompleteEvent {
	kind: "device-creation-complete";
	data: {
		uid: string
	};
}

interface SensorCreationCompleteEvent {
	kind: "sensor-creation-complete";
}

interface DeviceCropDisplayEvent {
	kind: "device-crop-display";
	data?: [number, number][];
}

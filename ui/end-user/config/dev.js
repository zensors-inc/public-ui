"use strict";

const autoprefixer = require("autoprefixer");
const webpack = require("webpack");

module.exports = (config) => {
	let loaders = config.module.rules[1].oneOf;

	let scssConfig = {
		test: /\.scss$/,
		use: [
			require.resolve('style-loader'),
			{
				loader: require.resolve('css-loader'),
				options: {
					importLoaders: 1,
				},
			},
			{
				loader: require.resolve("sass-loader")
			},
			{
				loader: require.resolve('postcss-loader'),
				options: {
					// Necessary for external CSS imports to work
					// https://github.com/facebookincubator/create-react-app/issues/2677
					ident: 'postcss',
					plugins: () => [
						require('postcss-flexbugs-fixes'),
						autoprefixer({
							browsers: [
								'>1%',
								'last 4 versions',
								'Firefox ESR',
								'not ie < 9', // React doesn't support IE8 anyway
							],
							flexbox: 'no-2009',
						}),
					],
				},
			},
		],
	};
	loaders.splice(loaders.length - 1, 0, scssConfig);

	let env = {
		NODE_ENV: "development",
		PUBLIC_URL: "",
		API_HOSTNAME: process.env.API_HOSTNAME || "figzbuild.andrew.cmu.edu",
		API_PORT: process.env.API_PORT || 443,
		API_PROTOCOL: process.env.API_PROTOCOL || "https",
		GQL_HOSTNAME: process.env.API_HOSTNAME || "figzbuild.andrew.cmu.edu",
		GQL_PORT: process.env.API_PORT || 443,
		GQL_HTTP_PROTOCOL: process.env.API_PROTOCOL || "https",
		GQL_WS_PROTOCOL: process.env.API_WS_PROTOCOL || "wss"
	};

	config.plugins[3] = new webpack.DefinePlugin({
		"process.env": Object.keys(env).reduce((out, key) =>
			Object.assign(out, { [key]: JSON.stringify(env[key]) }), {})
	});
};

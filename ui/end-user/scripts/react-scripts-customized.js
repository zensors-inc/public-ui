"use strict";

const rewire = require("rewire");

switch (process.argv[2]) {
	case "start":
		reconfigure("react-scripts-ts/scripts/start.js", "../config/dev.js");
		break;

	case "build":
		reconfigure("react-scripts-ts/scripts/build.js", "../config/prod.js");
		break;

	default:
		console.log("Only 'start' and 'build' are supported.");
		break;
}

function reconfigure(rsConfigPath, customConfigPath) {
	let rsConfigModule = rewire(rsConfigPath);
	let config = rsConfigModule.__get__("config");

	require(customConfigPath)(config);
}

function customConfig(path) {
	try {
		return require(path);
	} catch (e) {
		if (e.code !== "MODULE_NOT_FOUND") {
			throw e;
		}
	}

	return () => undefined;
}

function rewireModule(path, customizer) {
	let defaults = rewire(path);
	let config = defaults.__get__(config);
	customizer(config);
}

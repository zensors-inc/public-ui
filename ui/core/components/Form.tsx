"use strict";

import * as React from "react";

import * as EventEmitter from "event-emitter";

import Input, { inputSymbol } from "./Input";
import Submit, { submitSymbol } from "./Submit";

type Props<S> = {
	init: S;
	submit: (data: S) => any;
};

function elementIsInput<T, K extends keyof T>(elt: any): elt is Input<T, any, any> {
	return elt && elt.type && typeof elt.type !== "string" && elt.type.hasOwnProperty(inputSymbol);
}

function elementIsSubmit(elt: any): elt is Submit {
	return elt && elt.type && typeof elt.type !== "string" && elt.type.hasOwnProperty(submitSymbol);
}

export default class Form<S> extends React.Component<Props<S>, S> {
	private emitter: EventEmitter.Emitter;

	public constructor(props: Props<S>, context: any) {
		super(props, context);
		this.state = this.props.init;
		this.emitter = EventEmitter({});
	}

	public render() {
		let children = React.Children.map(this.props.children, (child) => {
			if (elementIsInput(child)) {
				return React.cloneElement(child as any, { // ?????
					data: child.props.getFormData(this.state),
					onChange: (value) => {
						child.props.setFormData(this.state, value);
						this.forceUpdate();
					}
				});
			} else if (elementIsSubmit(child)) {
				return React.cloneElement(child as any, { // ?????
					submit: () => this.submit()
				});
			} else {
				return child;
			}
		});

		return <div>{ children }</div>;
	}

	private submit(): void {
		this.props.submit(this.state);
	}
}

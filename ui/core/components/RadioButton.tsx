"use strict";

import * as React from "react";

import Input, { InputProps, inputSymbol } from "./Input";

import "./RadioButton.scss";

type Props<T, V> = {
	label?: string;
	options: { key: string, value: V }[];
	annotations?: {key: string, annotation: string}[]
};

type State = {
	hover?: string;
};

export default class RadioButton2<T, V> extends Input<T, V, V, Props<T, V>, State> {
	public static [inputSymbol] = true;

	public constructor(props: Props<T, V>, context: any) {
		super(props, context);
		this.state = {};
	}
	public render() {
		let items = this.props.options.map(({ key, value }, i) => {
			return (
				<div onMouseEnter={() => this.displayHoverAnnotation(key)}
					className="zs-radio"
					key={i}>
					<label onClick={() => this.handleRadioChange(i)}>
						<input className="zs-radio-input" type="radio" value={i}
								checked={this.props.data === value}/>
						<div className="zs-radio-check"></div>
						<label className="zs-radio-label">{key}</label>
					</label>
				</div>
			);
		});

		let selectedIndex = this.props.options.findIndex(({ key, value }) => value === this.props.data);

		return (
			<div className={(this.props.annotations)
					? "zs-radio-group zs-radio-annotation-group"
					: "zs-radio-group"}
				onMouseLeave={() => this.setState({hover: undefined})}>
				<div className="zs-radio-opts">
					{items}
				</div>

				{ this.state.hover ?
					<div className="zs-radio-annotation-column">
							<span className="zs-radio-annotation-arrow"/>
							<div className="zs-radio-annotation-content">
								{this.state.hover}
							</div>
					</div>
					: null
				}
			</div>
		);
	}

	private handleRadioChange = (i: number) => {
		if (this.props.onChange) {
			this.props.onChange(this.props.options[i].value);
		}
	}

	private displayHoverAnnotation = (key: string) => {
		if (this.props.annotations) {
			let matchingAnnotation = this.props.annotations.find((elem) => elem.key  === key);
			if (matchingAnnotation) {
				this.setState({hover: matchingAnnotation.annotation});
			}
		}

	}
}

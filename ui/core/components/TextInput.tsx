"use strict";

import * as React from "react";

import Input, { InputProps, inputSymbol } from "./Input";

import "./TextInput.scss";

export const enum TextInputStyle {
	NORMAL,
	WARNING,
	ERROR
}

type Props<T> = {
	placeholder?: string;
	label?: string;
	data?: string;
	disabled?: boolean;
	className?: string;
	style?: TextInputStyle;
	onClick?: () => void;
	onFocus?: (param: any) => void;
	onBlur?: (param: any) => void;
	type?: "text" | "password";
};

export default class TextInput<T> extends Input<T, string, string, Props<T>> {
	public static [inputSymbol] = true;

	public render() {

		let label = this.props.label
			? (<label className="zs-input-label" >{this.props.label}</label>)
			: null;

		return (
			<div className="zs-text-input">

				{label}

				<input
					type={this.props.type || "text"}
					className="zs-text-box"
					placeholder={this.props.placeholder}
					disabled={this.props.disabled ? true : false}
					value={this.props.data}
					onFocus={this.props.onFocus}
					onBlur={this.props.onBlur}
					onChange={(e) => {
						if (this.props.onChange) {
							this.props.onChange(e.target.value);
						}
					}}
					/>
			</div>
		);
	}
}

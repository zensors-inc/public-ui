"use strict";

import * as React from "react";

import Input, { InputProps, inputSymbol } from "./Input";

import "./Checkbox.scss";


/*  USAGE:
 *	bind checkbox state (boolean) with <ZsCheckbox data={here} />
 *  bind change handler to <ZsCheckbox onChange={f: (boolean) => any} />
 *  label is optional, passed in as props
 */

type Props<T, V> = {
	label?: string;
};

type State = {
	controlled: boolean;
	checked?: boolean;
};

export default class Checkbox<T, V> extends Input<T, boolean, boolean, Props<T, V>, State> {
	public static [inputSymbol] = true;

	public constructor(props: Props<T, V>, context: any) {
		super(props, context);
		if (typeof this.props.data !== "boolean") {
			this.state = {
				controlled: false,
				checked: false,
			};
		} else {
			this.state = {
				controlled: true,
			};
		}
	}

	public render() {
		return (
			<label className="zs-checkbox">
				{this.props.label}
				<input type="checkbox"
					className="zs-checkbox-input"
					checked={this.resolvedChecked()}
					onChange={() => this.handleCheckboxChange()}></input>
				<span className="zs-checkbox-box"></span>
				<span className="zs-checkbox-checkmark"></span>
			</label>
		);
	}

	private resolvedChecked() {
		if (typeof this.props.data !== "boolean") {
			return this.props.data;
		} else {
			return this.state.checked;
		}
	}

	private handleCheckboxChange() {
		if (this.props.onChange) {
			if (this.state.controlled) {
				this.props.onChange(!this.props.data);
			} else {
				let currentState = this.state.checked;
				this.setState({checked: !currentState});
				this.props.onChange(!currentState);
			}
		}
	}
}





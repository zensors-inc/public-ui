"use strict";

import * as React from "react";

import Checkbox from "./Checkbox";
import Input from "./Input";

type Props<T, V> = {
	header?: string;
	options: { key: string, value: V }[];
};

type State = {
	checkboxStates: boolean[];
};

export default class CheckboxGroup<T, V> extends Input<T, V, V[], Props<T, V>, State> {
	public constructor(props: Props<T, V>, context: any) {
		super(props, context);
		this.state = {
			checkboxStates: new Array<boolean>(this.props.options.length),
		};
	}

	public render() {
		let items = this.props.options.map(({ key, value}, i) => {
			return (
				<Checkbox
					key={i}
					label={key}
					onChange={(val) => this.handleCheckboxChange(i, val)}
				/>
			);
		});

		return (
			<div className="zs-checkbox-group">
				{ this.props.header ? <h3 className="h300"> {this.props.header} </h3> : null }
				{ items }
			</div>
		);
	}

	private handleCheckboxChange(i: number, val: boolean) {
		let checkboxStates = this.state.checkboxStates;
		checkboxStates[i] = val;
		this.setState({checkboxStates});

		if (this.props.onChange) {
			let result = [] as V[];
			for (let index in checkboxStates) {
				if (checkboxStates[index]) {
					result.push(this.props.options[index].value);
				}
			}
			this.props.onChange(result);
		}
	}
}

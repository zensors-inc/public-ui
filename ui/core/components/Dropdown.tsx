"use strict";

import { Button, Classes, InputGroup, Menu, MenuItem, Popover, Position } from "@blueprintjs/core";
import { IconName } from "@blueprintjs/icons";

import * as React from "react";

import Input, {
	InputProps,
	inputSymbol
} from "./Input";

type Props<V> = {
	leftIcon?: IconName;
	options: { key: string, value: V }[];
	data?: V;
};

type State = {};

export default class Dropdown<T, V> extends Input<T, V, V, Props<V>> {
	public static [inputSymbol] = true;

	public shouldComponentUpdate(nextProps: Props<V>, nextState: State) {
		return this.props.data !== nextProps.data;
	}

	public render() {
		let change =
			this.props.onChange
				? ((fn: (value: V) => void) => (value: V) => () => fn(value))(this.props.onChange)
				: (value: V) => () => undefined;

		let items =
			this.props.options.map(({ key, value }, i) => <MenuItem text={key} key={i} onClick={change(value)} />);

		let selected = this.props.options.filter(({ key, value }) => value === this.props.data)[0];

		return (
			<Popover content={<Menu>{items}</Menu>} position={Position.RIGHT_TOP}>
				<Button
					icon={this.props.leftIcon}
					text={selected === undefined ? "None" : selected.key}
				/>
			</Popover>
		);
	}
}

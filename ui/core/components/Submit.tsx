"use strict";

import * as React from "react";

import { Button } from "@blueprintjs/core";
import { IconName } from "@blueprintjs/icons";

export const submitSymbol = Symbol();

type Props = {
	rightIcon?: IconName;
	submit?: () => void;
	text: string;
};

export default class Submit extends React.Component<Props, {}> {
	public static [submitSymbol] = true;

	public render() {
		return (
			<Button
					rightIcon={this.props.rightIcon}
					onClick={this.props.submit}>
				{this.props.text}
			</Button>
		);
	}
}

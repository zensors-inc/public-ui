"use strict";

import * as React from "react";

import "./Button.scss";

export const enum ButtonStyle {
	PRIMARY,
	SECONDARY
}

type Props = {
	label: string;
	onClick?: () => void;
	disabled?: boolean;
	style?: ButtonStyle;
	className?: string;
};

export default class Button extends React.Component<Props, {}> {
	public render() {
		return (
			<button className={"zs-button " + this.style()}
					disabled={this.props.disabled ? true : false}
					onClick={this.props.onClick}>
				{this.props.label}
			</button>
		);
	}

	private style() {
		let classNames = this.props.className ? this.props.className : "";

		if (this.props.style) {
			switch (+this.props.style) {
				case ButtonStyle.PRIMARY:
					break;

				case ButtonStyle.SECONDARY:
					classNames += " zs-button-secondary";
					break;

				default:
					break;
			}
		}

		return classNames;
	}
}

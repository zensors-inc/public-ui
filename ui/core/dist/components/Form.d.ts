import * as React from "react";
declare type Props<S> = {
    init: S;
    submit: (data: S) => any;
};
export default class Form<S> extends React.Component<Props<S>, S> {
    private emitter;
    constructor(props: Props<S>, context: any);
    render(): JSX.Element;
    private submit;
}
export {};

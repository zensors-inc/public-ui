"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Input_1 = require("./Input");
require("./TextInput.scss");
var TextInput = /** @class */ (function (_super) {
    __extends(TextInput, _super);
    function TextInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextInput.prototype.render = function () {
        var _this = this;
        var label = this.props.label
            ? (React.createElement("label", { className: "zs-input-label" }, this.props.label))
            : null;
        return (React.createElement("div", { className: "zs-text-input" },
            label,
            React.createElement("input", { type: this.props.type || "text", className: "zs-text-box", placeholder: this.props.placeholder, disabled: this.props.disabled ? true : false, value: this.props.data, onFocus: this.props.onFocus, onBlur: this.props.onBlur, onChange: function (e) {
                    if (_this.props.onChange) {
                        _this.props.onChange(e.target.value);
                    }
                } })));
    };
    var _a;
    _a = Input_1.inputSymbol;
    TextInput[_a] = true;
    return TextInput;
}(Input_1.default));
exports.default = TextInput;

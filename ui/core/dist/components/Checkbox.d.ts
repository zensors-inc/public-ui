/// <reference types="react" />
import Input, { inputSymbol } from "./Input";
import "./Checkbox.scss";
declare type Props<T, V> = {
    label?: string;
};
declare type State = {
    controlled: boolean;
    checked?: boolean;
};
export default class Checkbox<T, V> extends Input<T, boolean, boolean, Props<T, V>, State> {
    static [inputSymbol]: boolean;
    constructor(props: Props<T, V>, context: any);
    render(): JSX.Element;
    private resolvedChecked;
    private handleCheckboxChange;
}
export {};

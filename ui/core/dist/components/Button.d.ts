import * as React from "react";
import "./Button.scss";
export declare const enum ButtonStyle {
    PRIMARY = 0,
    SECONDARY = 1
}
declare type Props = {
    label: string;
    onClick?: () => void;
    disabled?: boolean;
    style?: ButtonStyle;
    className?: string;
};
export default class Button extends React.Component<Props, {}> {
    render(): JSX.Element;
    private style;
}
export {};

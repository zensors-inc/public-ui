/// <reference types="react" />
import Input from "./Input";
declare type Props<T, V> = {
    header?: string;
    options: {
        key: string;
        value: V;
    }[];
};
declare type State = {
    checkboxStates: boolean[];
};
export default class CheckboxGroup<T, V> extends Input<T, V, V[], Props<T, V>, State> {
    constructor(props: Props<T, V>, context: any);
    render(): JSX.Element;
    private handleCheckboxChange;
}
export {};

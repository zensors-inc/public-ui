"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Checkbox_1 = require("./Checkbox");
var Input_1 = require("./Input");
var CheckboxGroup = /** @class */ (function (_super) {
    __extends(CheckboxGroup, _super);
    function CheckboxGroup(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.state = {
            checkboxStates: new Array(_this.props.options.length),
        };
        return _this;
    }
    CheckboxGroup.prototype.render = function () {
        var _this = this;
        var items = this.props.options.map(function (_a, i) {
            var key = _a.key, value = _a.value;
            return (React.createElement(Checkbox_1.default, { key: i, label: key, onChange: function (val) { return _this.handleCheckboxChange(i, val); } }));
        });
        return (React.createElement("div", { className: "zs-checkbox-group" },
            this.props.header ? React.createElement("h3", { className: "h300" },
                " ",
                this.props.header,
                " ") : null,
            items));
    };
    CheckboxGroup.prototype.handleCheckboxChange = function (i, val) {
        var checkboxStates = this.state.checkboxStates;
        checkboxStates[i] = val;
        this.setState({ checkboxStates: checkboxStates });
        if (this.props.onChange) {
            var result = [];
            for (var index in checkboxStates) {
                if (checkboxStates[index]) {
                    result.push(this.props.options[index].value);
                }
            }
            this.props.onChange(result);
        }
    };
    return CheckboxGroup;
}(Input_1.default));
exports.default = CheckboxGroup;

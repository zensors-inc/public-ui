/// <reference types="react" />
import Input, { inputSymbol } from "./Input";
import "./TextInput.scss";
export declare const enum TextInputStyle {
    NORMAL = 0,
    WARNING = 1,
    ERROR = 2
}
declare type Props<T> = {
    placeholder?: string;
    label?: string;
    data?: string;
    disabled?: boolean;
    className?: string;
    style?: TextInputStyle;
    onClick?: () => void;
    onFocus?: (param: any) => void;
    onBlur?: (param: any) => void;
    type?: "text" | "password";
};
export default class TextInput<T> extends Input<T, string, string, Props<T>> {
    static [inputSymbol]: boolean;
    render(): JSX.Element;
}
export {};

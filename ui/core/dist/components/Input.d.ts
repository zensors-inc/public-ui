import * as React from "react";
export declare const inputSymbol: unique symbol;
export declare type InputProps<T, V1, V2> = {
    getFormData?: (form: T) => V1;
    setFormData?: (form: T, data: V2) => void;
    onChange?: (data: V2) => void;
    data?: V1;
};
export default abstract class Input<T, V1, V2, P = {}, S = {}> extends React.Component<P & InputProps<T, V1, V2>, S> {
}

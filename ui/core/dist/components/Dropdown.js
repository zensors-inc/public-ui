"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@blueprintjs/core");
var React = require("react");
var Input_1 = require("./Input");
var Dropdown = /** @class */ (function (_super) {
    __extends(Dropdown, _super);
    function Dropdown() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Dropdown.prototype.shouldComponentUpdate = function (nextProps, nextState) {
        return this.props.data !== nextProps.data;
    };
    Dropdown.prototype.render = function () {
        var _this = this;
        var change = this.props.onChange
            ? (function (fn) { return function (value) { return function () { return fn(value); }; }; })(this.props.onChange)
            : function (value) { return function () { return undefined; }; };
        var items = this.props.options.map(function (_a, i) {
            var key = _a.key, value = _a.value;
            return React.createElement(core_1.MenuItem, { text: key, key: i, onClick: change(value) });
        });
        var selected = this.props.options.filter(function (_a) {
            var key = _a.key, value = _a.value;
            return value === _this.props.data;
        })[0];
        return (React.createElement(core_1.Popover, { content: React.createElement(core_1.Menu, null, items), position: core_1.Position.RIGHT_TOP },
            React.createElement(core_1.Button, { icon: this.props.leftIcon, text: selected === undefined ? "None" : selected.key })));
    };
    var _a;
    _a = Input_1.inputSymbol;
    Dropdown[_a] = true;
    return Dropdown;
}(Input_1.default));
exports.default = Dropdown;

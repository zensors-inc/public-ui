"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var EventEmitter = require("event-emitter");
var Input_1 = require("./Input");
var Submit_1 = require("./Submit");
function elementIsInput(elt) {
    return elt && elt.type && typeof elt.type !== "string" && elt.type.hasOwnProperty(Input_1.inputSymbol);
}
function elementIsSubmit(elt) {
    return elt && elt.type && typeof elt.type !== "string" && elt.type.hasOwnProperty(Submit_1.submitSymbol);
}
var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.state = _this.props.init;
        _this.emitter = EventEmitter({});
        return _this;
    }
    Form.prototype.render = function () {
        var _this = this;
        var children = React.Children.map(this.props.children, function (child) {
            if (elementIsInput(child)) {
                return React.cloneElement(child, {
                    data: child.props.getFormData(_this.state),
                    onChange: function (value) {
                        child.props.setFormData(_this.state, value);
                        _this.forceUpdate();
                    }
                });
            }
            else if (elementIsSubmit(child)) {
                return React.cloneElement(child, {
                    submit: function () { return _this.submit(); }
                });
            }
            else {
                return child;
            }
        });
        return React.createElement("div", null, children);
    };
    Form.prototype.submit = function () {
        this.props.submit(this.state);
    };
    return Form;
}(React.Component));
exports.default = Form;

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Input_1 = require("./Input");
require("./Checkbox.scss");
var Checkbox = /** @class */ (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox(props, context) {
        var _this = _super.call(this, props, context) || this;
        if (typeof _this.props.data !== "boolean") {
            _this.state = {
                controlled: false,
                checked: false,
            };
        }
        else {
            _this.state = {
                controlled: true,
            };
        }
        return _this;
    }
    Checkbox.prototype.render = function () {
        var _this = this;
        return (React.createElement("label", { className: "zs-checkbox" },
            this.props.label,
            React.createElement("input", { type: "checkbox", className: "zs-checkbox-input", checked: this.resolvedChecked(), onChange: function () { return _this.handleCheckboxChange(); } }),
            React.createElement("span", { className: "zs-checkbox-box" }),
            React.createElement("span", { className: "zs-checkbox-checkmark" })));
    };
    Checkbox.prototype.resolvedChecked = function () {
        if (typeof this.props.data !== "boolean") {
            return this.props.data;
        }
        else {
            return this.state.checked;
        }
    };
    Checkbox.prototype.handleCheckboxChange = function () {
        if (this.props.onChange) {
            if (this.state.controlled) {
                this.props.onChange(!this.props.data);
            }
            else {
                var currentState = this.state.checked;
                this.setState({ checked: !currentState });
                this.props.onChange(!currentState);
            }
        }
    };
    var _a;
    _a = Input_1.inputSymbol;
    Checkbox[_a] = true;
    return Checkbox;
}(Input_1.default));
exports.default = Checkbox;

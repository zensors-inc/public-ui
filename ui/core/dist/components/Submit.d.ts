import * as React from "react";
import { IconName } from "@blueprintjs/icons";
export declare const submitSymbol: unique symbol;
declare type Props = {
    rightIcon?: IconName;
    submit?: () => void;
    text: string;
};
export default class Submit extends React.Component<Props, {}> {
    static [submitSymbol]: boolean;
    render(): JSX.Element;
}
export {};

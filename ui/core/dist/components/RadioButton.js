"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Input_1 = require("./Input");
require("./RadioButton.scss");
var RadioButton2 = /** @class */ (function (_super) {
    __extends(RadioButton2, _super);
    function RadioButton2(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.handleRadioChange = function (i) {
            if (_this.props.onChange) {
                _this.props.onChange(_this.props.options[i].value);
            }
        };
        _this.displayHoverAnnotation = function (key) {
            if (_this.props.annotations) {
                var matchingAnnotation = _this.props.annotations.find(function (elem) { return elem.key === key; });
                if (matchingAnnotation) {
                    _this.setState({ hover: matchingAnnotation.annotation });
                }
            }
        };
        _this.state = {};
        return _this;
    }
    RadioButton2.prototype.render = function () {
        var _this = this;
        var items = this.props.options.map(function (_a, i) {
            var key = _a.key, value = _a.value;
            return (React.createElement("div", { onMouseEnter: function () { return _this.displayHoverAnnotation(key); }, className: "zs-radio", key: i },
                React.createElement("label", { onClick: function () { return _this.handleRadioChange(i); } },
                    React.createElement("input", { className: "zs-radio-input", type: "radio", value: i, checked: _this.props.data === value }),
                    React.createElement("div", { className: "zs-radio-check" }),
                    React.createElement("label", { className: "zs-radio-label" }, key))));
        });
        var selectedIndex = this.props.options.findIndex(function (_a) {
            var key = _a.key, value = _a.value;
            return value === _this.props.data;
        });
        return (React.createElement("div", { className: (this.props.annotations)
                ? "zs-radio-group zs-radio-annotation-group"
                : "zs-radio-group", onMouseLeave: function () { return _this.setState({ hover: undefined }); } },
            React.createElement("div", { className: "zs-radio-opts" }, items),
            this.state.hover ?
                React.createElement("div", { className: "zs-radio-annotation-column" },
                    React.createElement("span", { className: "zs-radio-annotation-arrow" }),
                    React.createElement("div", { className: "zs-radio-annotation-content" }, this.state.hover))
                : null));
    };
    var _a;
    _a = Input_1.inputSymbol;
    RadioButton2[_a] = true;
    return RadioButton2;
}(Input_1.default));
exports.default = RadioButton2;

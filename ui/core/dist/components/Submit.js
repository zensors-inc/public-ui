"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var core_1 = require("@blueprintjs/core");
exports.submitSymbol = Symbol();
var Submit = /** @class */ (function (_super) {
    __extends(Submit, _super);
    function Submit() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Submit.prototype.render = function () {
        return (React.createElement(core_1.Button, { rightIcon: this.props.rightIcon, onClick: this.props.submit }, this.props.text));
    };
    var _a;
    _a = exports.submitSymbol;
    Submit[_a] = true;
    return Submit;
}(React.Component));
exports.default = Submit;

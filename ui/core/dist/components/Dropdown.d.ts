/// <reference types="react" />
import { IconName } from "@blueprintjs/icons";
import Input, { inputSymbol } from "./Input";
declare type Props<V> = {
    leftIcon?: IconName;
    options: {
        key: string;
        value: V;
    }[];
    data?: V;
};
declare type State = {};
export default class Dropdown<T, V> extends Input<T, V, V, Props<V>> {
    static [inputSymbol]: boolean;
    shouldComponentUpdate(nextProps: Props<V>, nextState: State): boolean;
    render(): JSX.Element;
}
export {};

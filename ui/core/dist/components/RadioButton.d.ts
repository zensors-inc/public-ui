/// <reference types="react" />
import Input, { inputSymbol } from "./Input";
import "./RadioButton.scss";
declare type Props<T, V> = {
    label?: string;
    options: {
        key: string;
        value: V;
    }[];
    annotations?: {
        key: string;
        annotation: string;
    }[];
};
declare type State = {
    hover?: string;
};
export default class RadioButton2<T, V> extends Input<T, V, V, Props<T, V>, State> {
    static [inputSymbol]: boolean;
    constructor(props: Props<T, V>, context: any);
    render(): JSX.Element;
    private handleRadioChange;
    private displayHoverAnnotation;
}
export {};
